/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

/**
 *
 * @author Administrador
 */
public class Queens {
    static boolean isValid(int[] arr, int k){
        for(int i = 0; i<k; i++){
            if(arr[i] == arr[k]) return false;
            if(Math.abs(arr[i]-arr[k]) == Math.abs(k - i)) return false;
        }
        
        return true;
    }
    
    static void solutions(int[] arr, int k){
        int n = arr.length;
        if(k == n) {printArray(arr);} // Imprimir todo
        else {
            for(int i=0; i<n; i++){
                arr[k]=i;
                if(isValid(arr,k))
                    solutions(arr, k+1);
            }
        }
    }
    
    static void printArray(int[] arr){
        for(int i = 0; i<arr.length; i++){
            for(int j=0; j<arr.length; j++){
                if(arr[i]==j)
                    System.out.print("& ");
                else
                    System.out.print("* ");
            }
            System.out.println();
        }
        System.out.println();
    }   
    
    static void solutions(int n){
        int[] arr = new int[n];
        
        solutions(arr, 0);        
    }
    
    public static void Queens_main(String[] args) {
        int queens = 4;
        solutions(queens);
    }
}
