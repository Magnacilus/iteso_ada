/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;
import iteso_ada.utils.ArrayToTable;
import java.util.Scanner;

/**
 *
 * @author Administrador
 */
public class Tarea8_1 {
    static final int COIN_VALUE = 0, COIN_AMOUNT = 1;
    
    static int[][][] changeTable(int change, int[][] coins){       
        /* Populate Coins Table */
        int[][][] coinsTable = new int[coins.length][change+1][2];
        for(int coinIndex = 0; coinIndex < coins.length; coinIndex++){
            for(int currentChange = 0; currentChange < change + 1; currentChange++){                
                
                if(coinIndex==0){/* Assuming sorted coins and the first coin has a unit value */
                    int coins_amount = currentChange / coins[coinIndex][COIN_VALUE];
                    if(coins_amount <= coins[coinIndex][COIN_AMOUNT]){
                        coinsTable[coinIndex][currentChange][0] = coins_amount;
                        coinsTable[coinIndex][currentChange][1] = coins_amount;
                    }
                    else {
                        coinsTable[coinIndex][currentChange][0] = coins[coinIndex][COIN_AMOUNT];
                        coinsTable[coinIndex][currentChange][1] = coins[coinIndex][COIN_AMOUNT];
                    }
                } 
                else if(currentChange == coins[coinIndex][COIN_VALUE]){ /* Current change equals that current currency value */
                    coinsTable[coinIndex][currentChange][0] = 1;
                    coinsTable[coinIndex][currentChange][1] = 1;
                }
                else if(currentChange < coins[coinIndex][COIN_VALUE]){ /* Current change minor than current currency value */
                    coinsTable[coinIndex][currentChange][0] = coinsTable[coinIndex - 1][currentChange][0];
                    coinsTable[coinIndex][currentChange][1] = 0;
                }
                else{
                    int newCoinsAmount = coinsTable[coinIndex][currentChange - coins[coinIndex][COIN_VALUE]][0] + 1;
                    int newCoinsCount = coinsTable[coinIndex][currentChange - coins[coinIndex][COIN_VALUE]][1] + 1;
                    if(newCoinsAmount < coinsTable[coinIndex - 1][currentChange][0]){
                        if(newCoinsCount <= coins[coinIndex][COIN_AMOUNT]){
                            coinsTable[coinIndex][currentChange][0] = newCoinsAmount;
                            coinsTable[coinIndex][currentChange][1] = newCoinsCount;
                        }
                        else{
                            int bestCount = coinsTable[coinIndex - 1][currentChange][0];
                            int coinsCount = 0;
                            for(int j=1; j <= coins[coinIndex][COIN_AMOUNT]; j++){
                                if(coins[coinIndex][COIN_VALUE] * j  <= currentChange){
                                    int count = coinsTable[coinIndex - 1][currentChange - (coins[coinIndex][COIN_VALUE] * j)][0] + j;
                                    if(count < bestCount){
                                        bestCount = count;
                                        coinsCount = j;
                                    }
                                }
                            }
                            
                            coinsTable[coinIndex][currentChange][0] = bestCount;
                            coinsTable[coinIndex][currentChange][1] = coinsCount;
                        }
                    }
                    else{
                        coinsTable[coinIndex][currentChange][0] = coinsTable[coinIndex - 1][currentChange][0];
                        coinsTable[coinIndex][currentChange][1] = 0;
                    }
                }
            }
        }
        
        ArrayToTable.IntArrayToTable(coinsTable, coins, "coins_table");
        
        return coinsTable;
    }
    
    static void OptimalChange(int change, int[][] coins, int[][][]coinsTable){       
        /* Calculate change based on Coins Table */
        int remainingChange = change;
        int coinIndex = coins.length - 1;
        int[] usedCoins = new int[coins.length];       
        
        while(coinIndex >= 0){
            if(remainingChange > 0){
                if(coinIndex == 0){
                    usedCoins[coinIndex] = coinsTable[coinIndex][remainingChange][0];
                    remainingChange = 0; /* Just to exit, although it could have a value in case minimum value coins are not enough */
                }
                else if(coinsTable[coinIndex][remainingChange][0] < coinsTable[coinIndex - 1][remainingChange][0]){
                    usedCoins[coinIndex] = coinsTable[coinIndex][remainingChange][1];
                    remainingChange -= coins[coinIndex][COIN_VALUE] * usedCoins[coinIndex];
                    //if(usedCoins[coinIndex] >= coins[coinIndex][COIN_AMOUNT]){
                        System.out.println(Integer.toString(coins[coinIndex][COIN_VALUE]) + " " + Integer.toString(usedCoins[coinIndex]));
                        coinIndex--;
                    //}
                }
                else{
                    System.out.println(Integer.toString(coins[coinIndex][COIN_VALUE]) + " " + Integer.toString(usedCoins[coinIndex]));
                    coinIndex--;
                }
            }
            else{ /* Just for printing purposes */
                System.out.println(Integer.toString(coins[coinIndex][COIN_VALUE]) + " " + Integer.toString(usedCoins[coinIndex]));
                coinIndex--;
            }
        }
    }
    
    static void OptimalChanges(int change, int[][] coins, int[][][]coinsTable){       
        
        int optimalCoins = coinsTable[coinsTable.length - 1][change][0];
        
        //for(int c = coins.length - 1; c >= 0; c--){
            //int[] usedCoins = new int[coins.length];
            int coinsCount = 0;
            String usedCoins = "";
            int remainingChange = change;
            
            buildChange(change, coins, coinsTable, usedCoins, coins.length - 1, coinsCount, optimalCoins);                  
    }
    
    static void buildChange(int change, int[][] coins, int[][][] coinsTable, String usedCoins, int idx, int coinsCount, int optimalCoins){
        if(change > 0 && idx >= 0 && coinsCount <= optimalCoins){
            if(coins[idx][COIN_VALUE] <= change){
                String strCoings = "";
                if (usedCoins.length() > 0)
                    strCoings += " ";
                strCoings += Integer.toString(coins[idx][COIN_VALUE]);
                buildChange(change - coins[idx][COIN_VALUE], coins, coinsTable, usedCoins + strCoings, idx, coinsCount + 1, optimalCoins);                
                
                if(idx > 0 && coinsTable[idx - 1][change][0] <= coinsTable[idx][change][0]){
                    buildChange(change, coins, coinsTable, usedCoins, idx - 1, coinsCount, optimalCoins);
                }
            }
            else{
                buildChange(change, coins, coinsTable, usedCoins, idx - 1, coinsCount, optimalCoins);
            }
        }
        else if(usedCoins.length() > 0 && coinsCount <= optimalCoins) {
            System.out.println(usedCoins);
        }
    }
    
    public static void main(String[] args) {
        /* Dynamic Input */
        Scanner sc = new Scanner(System.in);
        int coinsLength = sc.nextInt();
        int change = sc.nextInt();
        
        int[][] coins = new int[coinsLength][2];
        
        for(int i = 0; i < coinsLength; i++){
            coins[i][COIN_VALUE] = sc.nextInt();
        }
        for(int i = 0; i < coinsLength; i++){
            //coins[i][COIN_AMOUNT] = sc.nextInt();
            /* Fixed to "unlimited */
            coins[i][COIN_AMOUNT] = 10000;
        }

        /* Static Input */
//        int coinsLength = 6;
//        int change = 113;
//        int[][] coins = {{1, 1000},
//                         {2, 1},
//                         {5, 1},
//                         {9, 4},
//                         {10, 3},
//                         {14, 2}
//                        };
        
        int[][][] changeTable = changeTable(change, coins);

        OptimalChange(change, coins, changeTable);        
        OptimalChanges(change, coins, changeTable);
        
   }
}
