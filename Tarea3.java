/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import iteso_ada.utils.arrays;
import iteso_ada.utils.search;
import iteso_ada.utils.sort;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Administrador
 */
public class Tarea3 {
    
    public static void TestBench() {
        final int INVOCATION_FACTOR = 500; //500
        final int MIN_N = 1;                    // Should not be lower than 1!
        final int MAX_N = 200; //200
        final int MIN_ARRAY_VALUE = 0;
        final int MAX_ARRAY_VALUE = 200;
        final int TOTAL_ITERATIONS = MAX_N * INVOCATION_FACTOR;
        
        PrintWriter writer = null;
        
        try {
            writer = new PrintWriter("DivideAndConquer.csv", "UTF-8");           
        }
        catch (IOException  ex) {
            System.out.println("Exception: " + ex);
            if(writer != null){
                writer.close();
            }
        }
                
        if(writer != null){
            writer.println("N,Iterations,Avg Comp RadixSort,Avg Mov RadixSort," + 
                           "Avg Comp Median,Avg Mov Median," + 
                           "Avg Comp QuickSort,Avg Mov QuickSort," + 
                           "Avg Comp MergeSort,Avg Mov MergeSort");
            System.out.println("...%");
        
            for(int n = MIN_N; n <= MAX_N; n++){
                long[] perfRadix = {0,0};
                long[] perfMeidan = {0,0};
                long[] perfQuick = {0,0};
                long[] perfMerge = {0,0};
                int[] performance = {0,0};
                
                int j = 0;
                while(j<(INVOCATION_FACTOR * n)) {
                    int[] arr = arrays.createIntArray(n, MIN_ARRAY_VALUE, MAX_ARRAY_VALUE);
                    
                    performance[0]=0;
                    performance[1]=0;
                    performance = sort.radix(arr.clone());
                    perfRadix[0] += performance[0];
                    perfRadix[1] += performance[1];
                    
                    performance[0]=0;
                    performance[1]=0;
                    int median = search.median(arr.clone(), performance);
                    perfMeidan[0] += performance[0];
                    perfMeidan[1] += performance[1];
                    
                    performance[0]=0;
                    performance[1]=0;
                    performance = sort.quicksort(arr.clone());
                    perfQuick[0] += performance[0];
                    perfQuick[1] += performance[1];
                    
                    performance[0]=0;
                    performance[1]=0;
                    sort.mergesort(arr.clone(), performance);
                    perfMerge[0] += performance[0];
                    perfMerge[1] += performance[1];
                    j++;
                }

                perfRadix[0] = perfRadix[0]/j;
                perfRadix[1] = perfRadix[1]/j;
                perfMeidan[0] = perfMeidan[0]/j;
                perfMeidan[1] = perfMeidan[1]/j;
                perfQuick[0] = perfQuick[0]/j;
                perfQuick[1] = perfQuick[1]/j;
                perfMerge[0] = perfMerge[0]/j;
                perfMerge[1] = perfMerge[1]/j;

                writer.println( n +","+ j + "," + 
                                perfRadix[0] + "," + perfRadix[1] + "," +
                                perfMeidan[0] + "," + perfMeidan[1] + "," +
                                perfQuick[0] + "," + perfQuick[1] + "," +
                                perfMerge[0] + "," + perfMerge[1]);

                System.out.println("n = " + n + "/" + MAX_N);
            }
            
            writer.close();
        }
    }
    
    public static void tarea3_main(String[] args) {
        //int N = 50;
        //int[] arr = arrays.createIntArray(N, -100, 1000);
        //int[] arr = {111116};
        //int[] arr = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        //int[] performance = {0, 0};
        
        //arrays.printArray(arr);
        
        //performance = sort.radix(arr);        
        //int media = search.median(arr, performance);        
        //performance = sort.quicksort(arr);        
        //arr = sort.mergesort(arr, performance);
        
        //arrays.printArray(arr);
        //System.out.println("N = " + N + " Comparisons = " +  performance[0] + " Swaps = " +  performance[1] + " Sorted: " + arrays.isSort(arr, true));
        //System.out.println("N = " + N + " Comparisons = " +  performance[0] + " Swaps = " +  performance[1] + " Medain: " + media);
        
        TestBench();
    }
    
}
