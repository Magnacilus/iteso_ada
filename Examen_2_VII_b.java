/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import java.util.Arrays;

/**
 *
 * @author Administrador
 */
public class Examen_2_VII_b {
    //feed forward
   static double getOutput(int[] input, double[] weights){
      double output = 0;
      for(int i=0; i< input.length; i++){
         output += input[i]* weights[i];
      }
      return output;
   }
   
   static byte activationFuncion (double output){
      return (byte) ( output > 0.5 ? 1: 0);
   }
   
   static void backpropagation(double error, int[]input, double [] weights){
      final double LEARNING_RATE  = 0.1;
      double delta = error * LEARNING_RATE;
      for(int i = 0; i< weights.length; i++)
         weights[i] += input[i]* delta;
   
   }
   
   static double[] trainPerceptron(int[][] trainingMatrix, int[] classes){
      double[] weights = new double[trainingMatrix[0].length];
      int currentRow = 0;
      int iteration = 0;
      int hits = 0; //cuantos han pasado la prueba
      
      while(trainingMatrix.length>hits){
         int[] currentInput = trainingMatrix[currentRow];
         double output = getOutput(currentInput, weights);
         byte af = activationFuncion(output);
         double error = classes[currentRow]-af;
         System.out.println(iteration + " : "+
           Arrays.toString(currentInput)+
           Arrays.toString(weights)+
           error);
         if(error == 0){
            hits++;
         }else{
            hits = 0;
            backpropagation(error, currentInput, weights);
         }
         currentRow++;
         iteration++;
         currentRow %= trainingMatrix.length;
         
      }
      
      return weights;
   }
   
   public static void main(String[] args) {
      int[][] trainingMatrix = {{3,2,2},
                           {5,6,7},
                           {10,9,8}
      };
      
      int[] classes = {2,32,360};
      
      double[] weights = trainPerceptron(trainingMatrix, classes);
      
      System.out.println("Weights: "+ Arrays.toString(weights));
      
      int o = activationFuncion(getOutput(new int[] { 30,29,28}, weights));
      System.out.println(o);
      
   }
}
