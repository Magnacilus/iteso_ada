/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import java.util.ArrayDeque;
import java.util.HashMap;

/**
 *
 * @author Administrador
 */
public class Graphs {
    public static boolean DFS(boolean[][] graph, int start, int end){
        ArrayDeque<Integer> pendingNodes = new ArrayDeque<Integer>();
        boolean[] processed = new boolean[graph.length];
        
        /**
         * Pushes an element onto the stack represented by this deque.
         */
        pendingNodes.push(start); //DFS
        
        while(!pendingNodes.isEmpty()){
            
            /**
             * Pops an element from the stack represented by this deque.
             */
            int node = pendingNodes.pop(); //DFS
            
            if(node == end){
                return true;
            }
            
            if(!processed[node]){                
                processed[node] = true;
                for(int i = 0; i<graph[node].length; i++){
                    if(graph[node][i]&&!processed[i]){
                        pendingNodes.push(i);  //DFS
                    }
                }
            }
        }
        
        return false;
    }
    
    public static boolean BFS(boolean[][] graph, int start, int end){
        ArrayDeque<Integer> pendingNodes = new ArrayDeque<Integer>();
        boolean[] processed = new boolean[graph.length];
        
        /**
         * Inserts the specified element at the end of this deque.
         */
        pendingNodes.offer(start); //BFS
        
        while(!pendingNodes.isEmpty()){
            
            /**
             * Retrieves and removes the head of the queue represented by this 
             *  deque (in other words, the first element of this deque), 
             *  or returns null if this deque is empty.
             */
            int node = pendingNodes.poll(); //BFS 
            
            if(node == end){
                return true;
            }
            
            if(!processed[node]){                
                processed[node] = true;
                for(int i = 0; i<graph[node].length; i++){
                    if(graph[node][i]&&!processed[i]){
                        pendingNodes.offer(i);  //BFS
                    }
                }
            }
        }
        
        return false;
    }
    
    public static void graphs_main(String[] args){
        String names[] = {"A", "B", "C", "D", "E", "F", "G"};
		boolean t = true, f = false;
       		boolean[][] graph = {   { f, t, t, f, f, f, f },
                                        { t, f, t, f, f, f, f },
                                        { t, t, f, f, f, f, f },
                                        { f, f, f, f, t, t, f },
                                        { f, f, f, t, f, f, t },
                                        { f, f, f, t, f, f, t },
                                        { f, f, f, f, t, t, f }
        };

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for(int i = 0; i < names.length; i++)
            map.put(names[i], i);

        System.out.println("DFS Search: " + DFS(graph, map.getOrDefault("D", -1), map.getOrDefault("G", -1)));
        System.out.println("BFS Search: " + BFS(graph, map.getOrDefault("D", -1), map.getOrDefault("G", -1)));
    }
}
