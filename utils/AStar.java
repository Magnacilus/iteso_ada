/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import java.util.HashMap;
import java.util.PriorityQueue;
/**
 *
 * @author Administrador
 */
public class AStar {
    static private HashMap<String, Block> blocks = new HashMap<String, Block>();   
    static private PriorityQueue<Block> openList = new PriorityQueue<Block>();
    static private PriorityQueue<Block> closedList = new PriorityQueue<Block>();
    
    static int MOVE_COST_STREIGHT = 10;
    static int MOVE_COST_DIAGONAL = 14;
    
    static class Block implements Comparable<Block>{
        private Block parent;
        final private int row;
        final private int column;
        final private boolean pathBlock;
        final private boolean start;        
        final private boolean target;
        final private int H;
        private int G;
        
        public Block(int pRrow, int pColumn, boolean pPathBlock, boolean pStart, boolean pTarget, int pH){
            row = pRrow;
            column = pColumn;
            pathBlock = pPathBlock;
            start = pStart;
            target = pTarget;
            H = pH;
        }
        
        public int get_Row(){
            return row;            
        }
        
        public int get_Column(){
            return column;            
        }
        
        public int get_H(){
            return H;            
        }
        
        public int get_G(){
            return G;            
        }
        public void set_G(int g){
            G = g;
        }
        
        public int get_F(){
            return H + G;
        }
        
        public void set_Parent(Block pParent){
            parent = pParent;
        }
        
        public Block get_Parent(){
            return parent;
        }
        
        public boolean isStrat(){
            return start;
        }

        public boolean isTarget(){
            return target;
        }
        
        public boolean IsPathBlock(){
            return pathBlock;
        }
        
        static public String BlockName(int pRow, int pColumn){
            return "[" + String.valueOf(pRow) + "," + String.valueOf(pColumn) + "]";
        }
                
        
        @Override
        public String toString(){
            return "[" + String.valueOf(row) + "," + String.valueOf(column) + "]";
        }
        
        @Override
        public int compareTo(Block other){
            int diff = get_F() - other.get_F();
            if(diff != 0)
                return diff;
            else
                return this.toString().compareTo(other.toString());
        }                        
    }
    
    static private void InitBlocks(boolean[][] grid, 
            int startR, int startC, 
            int targetR, int targetC){
        for(int r = 0; r < grid.length; r++){
            for(int c = 0; c < grid[r].length; c++){
                Block block = new Block(r,c,            // Column and Row
                        grid[r][c],                     // Path or blocked
                        r==startR && c==startC,         // Is Start point?
                        r==targetR && c==targetC,       // Is Target pint?
                        Math.abs(targetR - r) + Math.abs(targetC - c) // Target Distance
                );
                blocks.put(block.toString(), block);
            }
        }
    }
        
    static private Block EvaluateBlock(boolean[][] grid){
        Block block = openList.poll();
        closedList.offer(block);
        for(int r = block.get_Row() - 1; r <= block.get_Row() + 1; r++){
            if(r >= 0 && r < grid.length){
                for(int c = block.get_Column() - 1; c <= block.get_Column() + 1; c++){
                    if(c >= 0 && c < grid[r].length){
                        int diff = Math.abs(r - block.get_Row()) + Math.abs(c - block.get_Column());
                        if(diff > 0){
                            Block openBlock = blocks.get(Block.BlockName(r, c));
                            if(!closedList.contains(openBlock) && openBlock.IsPathBlock()){
                                int moveCost;
                                if(diff == 2){
                                    moveCost = MOVE_COST_DIAGONAL;
                                }
                                else if(diff == 1){
                                    moveCost = MOVE_COST_STREIGHT;
                                }
                                else{
                                    System.out.println("EvaluateBlock: Something weird has happened :-(");
                                    moveCost = 0;
                                }
                                boolean visited = openBlock.get_G() > 0;   // Block already visited
                                if(!visited){
                                    openBlock.set_G(block.get_G() + moveCost);
                                    openBlock.set_Parent(block);
                                    openList.offer(openBlock);
                                }
                                else if(block.get_G() + moveCost < openBlock.get_G()){
                                    openBlock.set_Parent(block);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return block;
    }
    
    static public String FindPath(boolean[][] grid, 
            int startR, int startC, 
            int targetR, int targetC){
        
        InitBlocks(grid, startR, startC, targetR, targetC);
        
        Block startBlock = blocks.get(Block.BlockName(startR, startC));
        openList.offer(startBlock);
        Block block;
        do{
            block = EvaluateBlock(grid);
        }while(!block.isTarget());
        
        String route = "";
        while(!block.isStrat()){
            route = " > " + block.toString() + route;
            block = block.parent;
        }
        
        route = block.toString() + route;
        
        return route;
    }
}
