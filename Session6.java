/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import iteso_ada.utils.arrays;
import iteso_ada.utils.binary_tree;
import iteso_ada.utils.binary_tree_234;
import iteso_ada.utils.Tree234;
import iteso_ada.utils.sort;

/**
 *
 * @author Administrador
 */
public class Session6 {
    public static void session6_main(String[] args) {
        int N = 1_000_000;
        //int[] arr = arrays.createSortedIntArray(N, 5);
        //int[] arr = arrays.createIntArray(N, 0, 2*N);
        //int[] arr = {3, 1, 5, 6, 2, 0, 7, 4};
        int[] arr = {8,5,3,9,12,6,7,0,10,4,15,13};
        //int[] performance;
        
        //arrays.printArray(arr);
        //int value = 11;
        //int index = search.binarySearchIte(arr, value);
        //int index = search.binarySearchInterpolation(arr, value);
        //int index = search.binaryTreeSearch(arr, value);
        //System.out.println("Value " + value + " Index = " + index);                
        
        //sort.mergeSortIte(arr);               
        //System.out.println("N = " + N + " Comparisons = " +  performance[0] + " Swaps = " +  performance[1]);                
        //arrays.printArray(arr);
        
        //binary_tree.Node root = binary_tree.createBinaryTree(arr);
        
        for(int i = 0; i<arr.length; i++){
            System.out.println((i+1) + " ----");
            int[] tmpArr = new int[i+1];
            System.arraycopy(arr, 0, tmpArr, 0, tmpArr.length);
            //binary_tree_234.Node234 root = binary_tree_234.createTree234(tmpArr);
            //binary_tree_234.printTree234(root, ".");            
            Tree234.Node234 root = Tree234.createTree234(tmpArr);
            Tree234.printTree234(root, "");
        }
        
        //binary_tree.print(root, 0);
        
    }
    
    public static int indexOf(int[] array, int value){
        for(int i=0; i<array.length; i++){
            if(array[i]==value)
                return i;
        }
        
        return 0;
    }
}
