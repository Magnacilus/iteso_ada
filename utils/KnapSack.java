/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Administrador
 */
public class KnapSack {
    public static class Item{
        public double value, weight, fraction;
        public Item(double v, double w){
            value = v;
            weight = w;
            fraction = 0;
        }        
    }
    
    static int select(List<Item> C){
        int index = -1;
        double maxVW = 0;
        for(int i=0; i<C.size(); i++){
            Item item = C.get(i);
            double vw = item.value / item.weight;
            if(vw > maxVW){
                maxVW = vw;
                index = i;
            }
        }
        return index;
    }
    
    public static List<Item> knapSack(List<Item> C, double maxWeight){
        List<Item> S = new ArrayList<Item>();
        double weight = 0;
        
        while(weight < maxWeight){
            int k = select(C);
            Item item = C.remove(k);            
            if(weight + item.weight <= maxWeight){
                item.fraction = 1.0;
                weight = weight + item.weight;
            }
            else{
                item.fraction = (maxWeight - weight)/item.weight;
                weight = maxWeight;                
            }
            S.add(item);            
        }
        
        return S;
    }
}
