/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Administrador
 */
public class ArrayToTable {
    public static void IntArrayToTable(int[][] array, String fileName){
        PrintWriter writer = null;
        
        try {
            writer = new PrintWriter(fileName + ".csv", "UTF-8");           
        }
        catch (IOException  ex) {
            System.out.println("Exception: " + ex);
            if(writer != null){
                writer.close();
            }
        }
        
        String line;
        int lineCount = 0;
        System.out.println("Writing lines... ");
        
        if(writer != null){
            /* Column Headers */
//            line = "-";
//            for(int i = 0; i < array[0].length; i++){
//                line += "," + "[" + Integer.toString(i) + "]";
//            }
//            writer.println(line);            
//            lineCount++;
            
            for(int i = 0; i < array.length; i++){
                //line = "[" + Integer.toString(i) + "]";
                line = Integer.toString(i) + ":";
                for(int j = 0; j < array[i].length; j++){
                    line += "," + Integer.toString(array[i][j]);
                }
                writer.println(line);            
                lineCount++;
            }
            
            lineCount++;        
            
            writer.close();
        }
    
        System.out.println("Lines written: " + lineCount);
    }
    
    public static void IntArrayToTable(int[][][] array, String fileName){
        PrintWriter writer = null;
        
        try {
            writer = new PrintWriter(fileName + ".csv", "UTF-8");           
        }
        catch (IOException  ex) {
            System.out.println("Exception: " + ex);
            if(writer != null){
                writer.close();
            }
        }
        
        String line;
        int lineCount = 0;
        System.out.println("Writing lines... ");
        
        if(writer != null){
            for(int z = 0; z < array[0][0].length; z++){
                /* Column Headers */
    //            line = "-";
    //            for(int i = 0; i < array[0].length; i++){
    //                line += "," + "[" + Integer.toString(i) + "]";
    //            }
    //            writer.println(line);            
    //            lineCount++;

                for(int i = 0; i < array.length; i++){
                    //line = "[" + Integer.toString(i) + "]";
                    line = Integer.toString(i) + ":";
                    for(int j = 0; j < array[i].length; j++){
                        line += "," + Integer.toString(array[i][j][z]);
                    }
                    writer.println(line);            
                    lineCount++;
                }
                
                writer.println("");
            }
            
            lineCount++;        
            
            writer.close();
        }
    
        System.out.println("Lines written: " + lineCount);
    }
    
    public static void IntArrayToTable(int[][][] array, int[][] xValues, String fileName){
        PrintWriter writer = null;
        
        try {
            writer = new PrintWriter(fileName + ".csv", "UTF-8");           
        }
        catch (IOException  ex) {
            System.out.println("Exception: " + ex);
            if(writer != null){
                writer.close();
            }
        }
        
        String line;
        int lineCount = 0;
        System.out.println("Writing lines... ");
        
        if(writer != null){
            for(int z = 0; z < array[0][0].length; z++){
                /* Column Headers */
//                line = "-";
//                for(int i = 0; i < array[0].length; i++){
//                    line += "," + "[" + Integer.toString(i) + "]";
//                }
//                writer.println(line);            
//                lineCount++;

                for(int i = 0; i < array.length; i++){
                    //line = "[" + Integer.toString(i) + "]";
                    line = Integer.toString(xValues[i][0]) + ":";
                    for(int j = 0; j < array[i].length; j++){
                        line += "," + Integer.toString(array[i][j][z]);
                    }
                    writer.println(line);            
                    lineCount++;
                }
                
                writer.println("");
            }
            
            lineCount++;        
            
            writer.close();
        }
    
        System.out.println("Lines written: " + lineCount);
    }
}
