/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import iteso_ada.utils.arrays;


/**
 *
 * @author Administrador
 */
public class Examen1 {
    public static void examen1_main(String[] args) {
        int N = 20;
        //int[] arr = arrays.createSortedIntArray(N, 5);
        //int[] arr = arrays.createIntArray(N, -100, 100);
        int[] arr = {1,2,3,4,5,6,7,8,9,10};
        //int[] arr = {9,8,7,5,1,3};
        //int[] arr = {9};
        //int[] performance;
        
        //arrays.printArray(arr);
        //int value = 11;
        //int index = search.binarySearchIte(arr, value);
        //int index = search.binarySearchInterpolation(arr, value);
        //int index = search.binaryTreeSearch(arr, value);
        //System.out.println("Value " + value + " Index = " + index);                
        
        //sort.mergeSortIte(arr);               
        //System.out.println("N = " + N + " Comparisons = " +  performance[0] + " Swaps = " +  performance[1]);                
        //arrays.printArray(arr);
        
        //binary_tree.Node root = binary_tree.createBinaryTree(arr);
        
        //binary_tree_234.Node234 root = binary_tree_234.
        
        //binary_tree.print(root, 0);  
        
        
        System.out.println("Max Sum Original = " + maxSum(arr));
        System.out.println("Max Sum Recursive = " + maxSum2(arr));
        //System.out.println("Is Heap = " + isHeap(arr));
        
        drawFractal(400, 300, 192); // El de la figura
    }
    
    public static int sum(int[] array, int start, int end) {
        int sum = 0;
        for(int i = start; i <= end; i ++) sum += array[i];
        return sum;
    }

    public static int maxSum(int[] array) {
        int maxSum = 0;
        for(int i = 0; i < array.length - 1; i ++) {
            for(int j = i + 1; j < array.length; j ++) {
                int s = sum(array, i, j);
                if(s > maxSum) maxSum = s;
            }
        }

        return maxSum;
    }
    
    public static int sum2(int[] array, int start, int end, int maxSum){
        int sumLeft = 0;
        int sumRight = 0;
        int length = end - start + 1;
        int sum = 0;
        int retSum = 0;
        
        if( length > 2){
            sumLeft = sum2(array, start, start + (length / 2) - 1, maxSum);
            sumRight = sum2(array, start + (length / 2), end, maxSum);
        }
        else if( length > 1){
            sumLeft = array[start];
            sumRight = array[end];
        }
        else {
            sumLeft = array[start];
        }
        
        sum = sumLeft + sumRight;
        
        if(sumLeft > sum)
            retSum = sumLeft;
        else if (sumRight > sum)
            retSum = sumRight;
        else
            retSum = sum;
        
        return retSum;
    }
    
    public static int maxSum2(int[] array){
        int maxSum = sum2(array, 0, array.length - 1, 0);
        return maxSum;
    }

    //-----------------------------------------
    private static int iGetParent(int index){
        return (index-1)/2;
    }
    
    private static int iGetLeftChild(int index){
        return (2 * index) + 1;
    }
    
    private static int iGetRigthChild(int index){
        return 2*(index + 2);
    }
    
    public static boolean checkHeap(int[] arr, int index){
        boolean ret = false;
        boolean retLeft = true;
        boolean retRight = true;
        
        int leftChild = iGetLeftChild(index);
        if(leftChild < arr.length){
            if(arr[index]<arr[leftChild]){
                return false;
            }
            retLeft = checkHeap(arr, leftChild);
        }
        
        int rightChild = iGetRigthChild(index);
        if(rightChild < arr.length){
            if(arr[index]<arr[rightChild]){
                return false;
            }
            retRight = checkHeap(arr, rightChild);
        }
        
        return retLeft && retRight;
    }
    
    public static boolean isHeap(int[] array){
        return checkHeap(array, 0);
    }
    
    //--------------------------------------------
    
    private static void drawCircle(int x, int y, int radius){
        //Stub
    }
    
    private static void drawTriangle(int x, int y, int radius){
        //Stub
    }
    
    // Recibe <x, y> del centro y radio del círculo 
    public static void drawFractal(int x, int y, int radius) {
        if(radius <= 4) return;
        drawCircle(x, y, radius);
        drawTriangle(x, y, radius);
        drawFractal(x + radius / 4, y - radius / 3, radius / 3); // Superior derecho
        drawFractal(x + radius / 4, y + radius / 3, radius / 3); // Inferior derecho
        drawFractal(x - radius / 3, y,  radius / 3); // Izquierdo
    }
    
    public static void drawFractalIte(int x, int y, int radius) {
        int radius4 = radius / 4;
        int radius3 = radius / 3;

        if(radius > 4){
            drawCircle(x, y, radius);
            drawTriangle(x, y, radius);

            while(radius3 > 4){                
                drawCircle(x + radius4, y - radius3, 
                            radius3); // Superior derecho
                drawTriangle(x + radius4, y - radius3, 
                            radius3); // Superior derecho
                
                
                drawCircle(x + radius4, y + radius3, 
                            radius3); // Inferior derecho
                drawTriangle(x + radius4, y + radius3, 
                            radius3); // Inferior derecho
                
                drawCircle(x - radius4, y, 
                            radius3); // Izquierdo
                drawTriangle(x - radius4, y, 
                            radius3); // Izquierdo
                
                radius4 = radius4 / 4;
                radius3 = radius3 / 3;
            }
        }
    }
    
    public static class Node{
        public Node child1;
        public Node child2;
        public Node child3;
        public int value1;
        public int value2;
    }
    
    public static boolean doSearch(Node n, int value){
        boolean ret = false;
        
        boolean retLeft = false;
        boolean retMiddle = false;
        boolean retRight = false;
        
        if(value == n.value1 || value == n.value2){
            ret = true;
        }
        else{
            if(n.child1 != null){
                retLeft = doSearch(n.child1, value);
            }
            if(n.child2 != null){
                retMiddle = doSearch(n.child2, value);
            }
            if(n.child3 != null){
                retLeft = doSearch(n.child3, value);
            }
            
            ret = retLeft || retMiddle || retRight;
        }
                    
        return ret;
    }
    
    public static void search(Node n, int value){
        if(doSearch(n, value)){
            System.out.println("Encontrado");
        }
    }

    
}
