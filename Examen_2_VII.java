/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import java.util.ArrayDeque;
import java.util.HashMap;

/**
 *
 * @author Administrador
 */
public class Examen_2_VII {
    public static final int MODULUS = 57413;
    public static HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
    final static int HASH_LIST_SIZE = 1000*1000*1000;
    
    public static int hashCode(int[] codeValues){
        int h = codeValues[0];
        for(int i = 1; i<codeValues.length; i++){
            h = (h*1000 + codeValues[i]) % HASH_LIST_SIZE;
        }
        return h;
    }
    
    /* Efficient - reusing previous calculations */
    public static int superFunction(int x, int y, int z){
        int result = 0;
        if(z > 0 && y > 0 && x > 0){
            int h = hashCode(new int[]{x, y, z});
            if(map.containsKey(h)){
                result = map.get(h);
            }
            else{
                result = ( 1 +
                        superFunction(x-2, y-1, z-1) + 
                        superFunction(x-1, y-2, z-1) + 
                        superFunction(x-1, y-1, z-2)) % MODULUS;
                
                map.put(h, result);                
            }
        }
        
        return result;
    }
    
    /* Not efficient */
    public static int superF(int x, int y, int z){
        int res = 0;
        ArrayDeque<Integer> stack = new ArrayDeque<Integer>();        
        stack.push(x);
        stack.push(y);
        stack.push(z);
        
        while(!stack.isEmpty()){
            int sz = stack.pop();
            int sy = stack.pop();
            int sx = stack.pop();
            
            if(sz > 0 && sy > 0 && sx > 0){
                res = (res + 1) % MODULUS;
                /*F(x-2,y-1,z-1)*/
                stack.push(sx - 2);
                stack.push(sy - 1);
                stack.push(sz - 1);
                /*F(x-1,y-2,z-1)*/
                stack.push(sx - 1);
                stack.push(sy - 2);
                stack.push(sz - 1);
                /*F(x-1,y-1,z-2)*/
                stack.push(sx - 1);
                stack.push(sy - 1);
                stack.push(sz - 2);
            }            
        }
        
        return res;
    }
    public static void main(String[] args) {       
        int x, y, z;
               
        System.out.printf("F(%d,%d,%d) = %d\n", x=5,y=6,z=7, superFunction(x,y,z));
        System.out.printf("F(%d,%d,%d) = %d\n", x=10,y=9,z=8, superFunction(x,y,z));
        System.out.printf("F(%d,%d,%d) = %d\n", x=30,y=29,z=28, superFunction(x,y,z));
        System.out.printf("F(%d,%d,%d) = %d\n", x=95,y=96,z=97, superFunction(x,y,z));
        System.out.printf("F(%d,%d,%d) = %d\n", x=100,y=100,z=100, superFunction(x,y,z));
    }
}
