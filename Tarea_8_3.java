/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Administrador
 */
public class Tarea_8_3 {
    public static double[] values;
    
    static class ExtraPoints implements Comparable<ExtraPoints>{
        public List<Double> items;
        
        ExtraPoints(double firstItem){
            items = new ArrayList<Double>();
            items.add(firstItem);
        }
        
        public void AddItem(double item){
            if(item > items.get(items.size() - 1))
                items.add(item);
        }
        
        @Override
        public int compareTo(ExtraPoints ep){
            if(items.size() < ep.items.size())
                return -1;
            if(items.size() > ep.items.size())
                return 1;
            return 0;
        }
        
        @Override
        public String toString(){
            String strExtraPoints = "";
            for(Double item : items){
                strExtraPoints += Double.toString(item) + " ";
            }
            return strExtraPoints;
        }
    }
    
    public static void getExtraPoints(){
        List<ExtraPoints> extraPoints = new ArrayList<ExtraPoints>();
        
        for(int i = 0; i < values.length; i++){
            ExtraPoints ep = new ExtraPoints(values[i]);            
            for(int j = i; j < values.length; j++){
                ep.AddItem(values[j]);
            }
            extraPoints.add(ep);
        }
        
        Collections.sort(extraPoints);
        
        System.out.println(extraPoints.get(extraPoints.size() - 1).toString());
    }
    
    public static void getExtraPoints2(){
        ArrayList<ArrayList<Integer>> biggers = new ArrayList<ArrayList<Integer>>();        
        for(int i = 0; i < values.length; i++){
            biggers.add( new ArrayList<Integer>());
            for(int j = i; j < values.length; j++){
                if(values[i] < values[j]){
                    biggers.get(i).add(j);
                }
            }
        }
        
        List<ExtraPoints> extraPointsList = new ArrayList<ExtraPoints>();
        for(int i = 0; i < biggers.size(); i++){
            ExtraPoints ep = new ExtraPoints(values[i]);
            int index = i;
            int nextItem = i;            
            while(biggers.get(nextItem).size()>0){
                for(Integer item : biggers.get(index)){
                    if(nextItem == index)
                        nextItem = item;
                    else if(biggers.get(nextItem).size() < biggers.get(item).size())
                        nextItem = item;
                }
                
                index = nextItem;
                ep.AddItem(values[nextItem]);
            }
            
            extraPointsList.add(ep);
        }
        
        Collections.sort(extraPointsList);        
        System.out.println(extraPointsList.get(extraPointsList.size() - 1).toString());
    }
    
    public static List<ArrayList<Double>> list;
    
    public static void CalculateExtraPoints(){
        list = new ArrayList<ArrayList<Double>>(values.length);
        for(int i = 0; i<values.length; i++)
            list.add(null);
        
        ArrayList<Double> extraPoints = new ArrayList<Double>();
        for(int i = 0; i<values.length; i++){
            ArrayList<Double> ep = getBiggers(i);
            if(ep.size() > extraPoints.size()){
                extraPoints = ep;
            }
        }
        
        String strExtraPoints = "";
        for(int i = extraPoints.size() - 1; i >= 0; i--){
            //strExtraPoints += Double.toString(extraPoints.get(i)) + " ";
            System.out.printf("%.3f ", extraPoints.get(i));
        }
        //System.out.println(strExtraPoints);
    }
    
    public static ArrayList<Double> getBiggers(int index){        
        ArrayList<Double> previous = new ArrayList<Double>();
        
        if(list.get(index) == null ){
            ArrayList<Double> biggers = new ArrayList<Double>();
            for(int i = index + 1; i < values.length; i++){
                if(values[i] > values[index]){
                    biggers = (ArrayList<Double>)getBiggers(i).clone();
                    if (biggers.size() > previous.size()){
                        previous = (ArrayList<Double>)biggers.clone();
                    }
                }
            }        

            previous.add(values[index]);        
            list.set(index, previous);
        }
        else{
            previous = list.get(index);
        }
        
        return previous;
    }
        
    public static void main(String[] args) {
        /* Dynamic Input */
        Scanner sc = new Scanner(System.in);        
        
        int length = sc.nextInt();
        values = new double[length];
        
        for(int i = 0; i < values.length; i++){
            values[i] = sc.nextDouble();
        }
        
        CalculateExtraPoints();
    }
}
