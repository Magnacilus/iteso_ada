/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import java.util.Scanner;

/**
 *
 * @author Administrador
 */
public class Tarea8_2 {
    
    public static void getChangeCombinations(int[] coins, int change, int coinsAmount){
        int transition = coinsAmount;
        int coinIndx = coins.length - 1;
        int j = 0;
        int remaining = change;
        int[] combination = new int[coinsAmount];
        while(transition > 0){
            while(j < coinsAmount && coinIndx >= 0 && remaining > 0){
                combination[j] = coins[coinIndx];
                remaining -= coins[coinIndx];
                
                if(remaining > 0){
                    /* Is there still a change to complete the remining change with current currency?*/
                    if(coins[coinIndx] * (combination.length - (j + 1)) >= remaining){
                        /* If remaining change lower than current currenty, try to lower the currency value */
                        if(remaining < coins[coinIndx]){
                            /* Is there still a change to complete the remining change with a lower currency?*/
                            if(coinIndx > 0 && coins[coinIndx - 1] * (combination.length - (j + 1)) >= remaining){
                                int k = coinIndx;
                                while(k >= 0 && remaining < coins[k]){
                                    k--;
                                }

                                if(k < 0){
                                    /* No chance to complete change with current combination, rever current coin and try with a lower one*/
                                    combination[j] = 0;
                                    remaining += coins[coinIndx];
                                    coinIndx--;
                                    j--;
                                }
                                else{
                                    coinIndx = k;
                                }
                            }
                            else
                            {
                                /* No change to complete the remaining change break the search */
                                combination[j] = 0;
                                remaining += coins[coinIndx];
                                j--;
                                
                                int index = getCoinIndex(coins, combination[j]);
                                if(index > 0){
                                    combination[j] = 0;
                                    remaining += coins[index];
                                    coinIndx = index - 1;
                                    j--;
                                }
                                else{
                                    System.out.println("Can not go back 1");
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        /* No change to complete the remaining change break the search */
                        combination[j] = 0;
                        remaining += coins[coinIndx];
                        j--;

                        int index = getCoinIndex(coins, combination[j]);
                        if(index > 0){
                            combination[j] = 0;
                            remaining += coins[index];
                            coinIndx = index - 1;
                            j--;
                        }
                        else{
                            System.out.println("Can not go back 1");
                            break;
                        }
                    }
                }
                else if(remaining == 0){
                    /* Change completed, but with the expected coins? */
                    if(j < coinsAmount - 1){
                        /* More coins needed, then revert current coin, and try with a lower one */
                        combination[j] = 0;
                        remaining += coins[coinIndx];
                        coinIndx--;
                        j--;
                    }
                }
                else { /* remaining < 0 */
                    /* Currency bigger than change, revert currency and try with a lower one */
                    combination[j] = 0;
                    remaining += coins[coinIndx];
                    coinIndx--;
                    j--;
                }
                
                j++;
            }
            
            if(j == coinsAmount && remaining == 0){
                printArray(combination);
            }
            
            int newTransition = transition;
            for(int t = combination.length - 1; t >= transition; t--){
                remaining += combination[t];
                combination[t] = 0;
            }            
            for(int k = transition - 1; k > 0 && newTransition == transition; k--){
                if(combination[k - 1] > combination[k]){
                    newTransition = k;
                    j = k;
                    int index = getCoinIndex(coins, combination[k - 1]);
                    if(index > 0){
                        remaining += combination[k - 1];
                        combination[k - 1] = coins[index - 1];
                        remaining -= combination[k - 1];
                        coinIndx = index - 1;
                    }
                    else
                    {
                        System.out.println("Index Error A");
                    }
                }
                remaining += combination[k];
                combination[k] = 0;                
            }
            
            if(newTransition < transition)
                transition = newTransition;
            else
                break;
        }
    }
    
    public static void printArray(int[] combination){
        String strCombination = "";
        for(int k = 0; k < combination.length; k++){
            if(strCombination.length() > 0)
                strCombination += " ";
            strCombination += Integer.toString(combination[k]);
        }

        System.out.println(strCombination);
    }
    
    public static int getCoinIndex(int[] coins, int coinValue){
        for(int i = 0; i<coins.length; i++)
            if(coins[i] == coinValue)
                return i;        
        return -1;
    }
    
    public static int[] coins;
    public static int[] change;
    
    public static void getChange(int coinIndex, int changeIndex, int amount){
        if( amount > 0 && changeIndex < change.length){
            while(coinIndex >= 0 &&
                  (coins[coinIndex] * (change.length - changeIndex)) >= amount){
                if(coins[coinIndex] <= amount){
                    change[changeIndex] = coins[coinIndex];
                    getChange(coinIndex, changeIndex + 1, amount - coins[coinIndex]);
                }
                coinIndex--;
            }
        }
        else if (amount == 0 && changeIndex == change.length){
            printArray(change);
        }
    }

    public static void main(String[] args) {
        /* Dynamic Input */
        Scanner sc = new Scanner(System.in);
        int coinsValues = sc.nextInt();
        int changeValue = sc.nextInt();
        int coinsAmount = sc.nextInt();
        coins = new int[coinsValues];
        change = new int[coinsAmount];
        
        for(int i = 0; i < coins.length; i++){
            coins[i] = sc.nextInt();
        }
       
        //getChangeCombinations(coins, change, coinsAmount);
        getChange(coins.length - 1, 0, changeValue);
    }
}
