/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;

import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;
import static org.graphstream.algorithm.Toolkit.*;
import org.graphstream.ui.view.*;
import org.graphstream.ui.spriteManager.*;
import org.graphstream.ui.swingViewer.ViewPanel;

/**
 *
 * @author Administrador
 */
public class GraphStreamExamples {
    public static void Example1(){
        Graph graph = new SingleGraph("Tutorial 1");
        
//        node = graph.addNode("A1" );
//        node = graph.addNode("A2" );
//        node = graph.addNode("A3" );
//        node = graph.addNode("A4" );
//        node = graph.addNode("A5" );
//        node = graph.addNode("A6" );
//        node = graph.addNode("A7" );
//        node = graph.addNode("B1" );
//        node = graph.addNode("B2" );
//        node = graph.addNode("B3" );
//        node = graph.addNode("B4" );
//        node = graph.addNode("B5" );
//        node = graph.addNode("B6" );
//        node = graph.addNode("B7" );
//        node = graph.addNode("C1" );
//        node = graph.addNode("C2" );
//        node = graph.addNode("C3" );
//        node = graph.addNode("C4" );
//        node = graph.addNode("C5" );
//        node = graph.addNode("C6" );
//        node = graph.addNode("C7" );
        graph.setStrict(false);
        graph.setAutoCreate( true );
        
        graph.addEdge("A4A5", "A4", "A5");
        graph.addEdge("A4B3", "A4", "B3"); graph.addEdge("A5B6", "A5", "B6");
        graph.addEdge("B3B4", "B3", "B4"); graph.addEdge("B4B5", "B4", "B5"); graph.addEdge("B5B6", "B5", "B6");
        graph.addEdge("B3C2", "B3", "C2"); graph.addEdge("B6C7", "B6", "C7");
        graph.addEdge("C2C3", "C2", "C3"); graph.addEdge("C3C4", "C3", "C4"); graph.addEdge("C4C5", "C4", "C5"); graph.addEdge("C5C6", "C5", "C6"); graph.addEdge("C6C7", "C6", "C7");        
        graph.display();
    }
    
    public static void Example2(){
        Graph graph = new SingleGraph("Tutorial 1");
        SpriteManager sman = new SpriteManager(graph);
        Node node;
        
        node = graph.addNode("A1" );
        node.setAttribute("xyz", 0, 2, 0);
        node = graph.addNode("A2" );
        node.setAttribute("xyz", 1, 2, 0);
        node = graph.addNode("A3" );
        node.setAttribute("xyz", 2, 2, 0);
        node = graph.addNode("A4" );
        node.setAttribute("xyz", 3, 2, 0);        
        node = graph.addNode("A5" );
        node.setAttribute("xyz", 4, 2, 0);        
        node = graph.addNode("A6" );
        node.setAttribute("xyz", 5, 2, 0);        
        node = graph.addNode("A7" );
        node.setAttribute("xyz", 6, 2, 0);        
        
        node = graph.addNode("B1" );
        node.setAttribute("xyz", 0, 1, 0);
        node = graph.addNode("BA2" );
        node.setAttribute("xyz", 1, 1, 0);
        node = graph.addNode("B3" );
        node.setAttribute("xyz", 2, 1, 0);
        node = graph.addNode("B4" );
        node.setAttribute("xyz", 3, 1, 0);        
        node = graph.addNode("B5" );
        node.setAttribute("xyz", 4, 1, 0);        
        node = graph.addNode("B6" );
        node.setAttribute("xyz", 5, 1, 0);        
        node = graph.addNode("B7" );
        node.setAttribute("xyz", 6, 1, 0);
        
        node = graph.addNode("C1" );
        node.setAttribute("xyz", 0, 0, 0);
        node = graph.addNode("C2" );
        node.setAttribute("xyz", 1, 0, 0);
        node = graph.addNode("C3" );
        node.setAttribute("xyz", 2, 0, 0);
        node = graph.addNode("C4" );
        node.setAttribute("xyz", 3, 0, 0);        
        node = graph.addNode("C5" );
        node.setAttribute("xyz", 4, 0, 0);        
        node = graph.addNode("C6" );
        node.setAttribute("xyz", 5, 0, 0);        
        node = graph.addNode("C7" );
        node.setAttribute("xyz", 6, 0, 0);
        
        
        Edge edge = graph.addEdge("A4A5", "A4", "A5");
        Sprite sA4A5 = sman.addSprite("S1"); sA4A5.setPosition(0.5); sA4A5.attachToEdge("A4A5");  sA4A5.addAttribute("text", "sA4A5 { color: red; }");
        
        graph.addEdge("A4B3", "A4", "B3"); graph.addEdge("A5B6", "A5", "B6");
        graph.addEdge("B3B4", "B3", "B4"); graph.addEdge("B4B5", "B4", "B5"); graph.addEdge("B5B6", "B5", "B6");
        graph.addEdge("B3C2", "B3", "C2"); graph.addEdge("B6C7", "B6", "C7");
        graph.addEdge("C2C3", "C2", "C3"); graph.addEdge("C3C4", "C3", "C4"); graph.addEdge("C4C5", "C4", "C5"); graph.addEdge("C5C6", "C5", "C6"); graph.addEdge("C6C7", "C6", "C7");        
        
        graph.addAttribute("ui.stylesheet", "graph { fill-color: red; }");
        
        Viewer  viewer = graph.display(false);
        //viewer.disableAutoLayout();
        //viewer.enableAutoLayout();
        
    }            
    
    public static void Example3(){
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        Graph graph = new MultiGraph("Le Havre");
		
        try {
                graph.read("LeHavre.dgs");
        } catch(Exception e) {
                e.printStackTrace();
                System.exit(1);
        }
        
        for(Edge edge: graph.getEachEdge()) {
            if(edge.hasAttribute("isTollway")) {
                edge.addAttribute("ui.class", "tollway");
            } else if(edge.hasAttribute("isTunnel")) {
                edge.addAttribute("ui.class", "tunnel");
            } else if(edge.hasAttribute("isBridge")) {
                edge.addAttribute("ui.class", "bridge");
            }
            
            double speedMax = edge.getNumber("speedMax") / 130.0;
            edge.setAttribute("ui.color", speedMax);
        }
        
        SpriteManager sman = new SpriteManager(graph);
        Sprite s1 = sman.addSprite("S1");
        Sprite s2 = sman.addSprite("S2");
        Node n1 = randomNode(graph);
        Node n2 = randomNode(graph);
        double p1[] = nodePosition(n1);
        double p2[] = nodePosition(n2);
        s1.setPosition(p1[0], p1[1], p1[2]);
        s2.setPosition(p2[0], p2[1], p2[2]);

        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");

        String styleSheet = FileReader("LeHavre.css");
        graph.addAttribute("ui.stylesheet", styleSheet);
        Viewer viewer = graph.display(false);
        ViewPanel view = viewer.getDefaultView();
        
        view.resizeFrame(800, 600);
        //view.setViewCenter(440000, 2503000, 0);
        //view.setViewPercent(0.25);
        
        graph.addAttribute("ui.screenshot", "LeHavre_screenshot.png");
    }
    
    public static String FileReader(String fileName) {

        // This will reference one line at a time
        String line = null;
        String styleSheet = "";

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = 
                new FileReader(fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                styleSheet += line;
            }   

            // Always close files.
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");                  
            // Or we could just do this: 
            // ex.printStackTrace();
        }
        
        return styleSheet;
    }
}
