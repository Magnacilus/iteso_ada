/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import java.util.List;
import java.util.PriorityQueue;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author Usuario
 */
public class MST {
    static double INFINIT = Double.MAX_VALUE;
    static String names[] = {"A", "B", "C", "D", "E", "F", "G"};
    static HashMap<String, Integer> map = new HashMap<String, Integer>();
    static {
        for(int i = 0; i<names.length; i++)
            map.put(names[i], i);
    }
    
//---------------------------------------    
    static class Edge implements Comparable<Edge>{
        int v1, v2;
        double weight;
        
        public Edge(int v1, int v2, double weight){
            this.v1 = v1;
            this.v2 = v2;
            this.weight = weight;
        }
        
        @Override
        public int compareTo(Edge e){
            //Negativo si el arista es menor que e
            if(this.weight<e.weight)
                return -1;
            //positivo si el arista es mayor a e
            if(this.weight>e.weight)
                return 1;
            //If they are equal
            return 0; //Si son iguales
        }
        
        public String toString(){
            return "(" + names[this.v1]+","+names[this.v2]+")_<"+weight+">";
        }
    }

    public static List<Edge> mstPrim(double[][] graph){
        // Almacenará las aristas que pertenecen al MST
        ArrayList<Edge> mst = new ArrayList<Edge>(graph.length - 1);

        // Almacenar los nodos que fueron incorporados al MST
        boolean visited[] = new boolean[graph.length];
        //Almacenará las aristas candidatas a pertencer al MST
        PriorityQueue<Edge> candidates = new PriorityQueue<Edge>();

        //This algorithm starts with Node 0
        visited[0] = true;
        // Add neightbors of Node 0
        for(int i = 1; i<graph[0].length; i++){
            if(graph[0][i] < INFINIT)
                candidates.add(new Edge(0,i,graph[0][i]));
        }

        // Ciclo que se repite N-1 Veces (Solo existirán N-1 aristas
        while(mst.size()<graph.length -1){
            // Sacar la arista candidata de menor peso
            Edge minEdge = candidates.poll();
            // Si conecta con un nodo del MST con otro fuera
            if(visited[minEdge.v1]!=visited[minEdge.v2]){
                mst.add(minEdge);
                //Añadir nodo a la lista de vistados
                int newNode = !visited[minEdge.v1]?minEdge.v1:minEdge.v2;
                visited[newNode] = true;
                // Añadir todos las aristas que parten del nuevo nodo
                for(int j=0; j<graph[newNode].length; j++){
                    if(graph[newNode][j]<INFINIT)
                        candidates.add(new Edge(newNode, j, graph[newNode][j]));
                }
            }
        }

        return mst;
    }
//---------------------------------------    
    public static int findLeader(int[] parents, int x){
        while(parents[x]!=x)
            x = parents[x];
        return x;
    }
    
    public static boolean join(int[] parents, int[] rank, int i, int j){
        int leaderI = findLeader(parents, i);
        int leaderJ = findLeader(parents, j);
        
        if(leaderI == leaderJ){
            return false;
        }
        
        int rankLeaderI = rank[leaderI];
        int rankLeaderJ = rank[leaderJ];
        
        if(rankLeaderI>rankLeaderJ || 
                rankLeaderI == rankLeaderJ && leaderI>leaderJ){
            parents[leaderJ] = leaderI;
            rank[leaderI] += rank[leaderJ];
        }else{
            parents[leaderI] = leaderJ;
            rank[leaderJ] += rank[leaderI];
        }
        
        return true;
    }
    
    public static List<Edge> kruskal (double[][] graph){
        PriorityQueue<Edge> queue = new PriorityQueue<Edge>();
        List<Edge> mst = new ArrayList<Edge>(graph.length-1);
        //Inicializar todos los edges
        for(int i=0; i < graph.length; i++){
            for(int j = i+1; j<graph[0].length; j++){
                Edge e = new Edge(i,j,graph[i][j]);
                queue.offer(e);
            }
        }
        
        int[] parents = new int[graph.length];
        int[] rank = new int[graph.length];
        for(int i = 0; i<parents.length; i++){
            parents[i]=i;
            rank[i] = 1;
        }
        
        while(mst.size() < graph.length-1){
            Edge minEdge = queue.poll();
            if(join(parents, rank, minEdge.v1, minEdge.v2)){
                mst.add(minEdge);
            }
        }
        
        return mst;
    }

    public static int[] kruskal_groups(double[][] graph){
        PriorityQueue<Edge> queue = new PriorityQueue<Edge>();
        List<Edge> mst = new ArrayList<Edge>(graph.length-1);
        //Inicializar todos los edges
        for(int i=0; i < graph.length; i++){
            for(int j = i+1; j<graph[0].length; j++){
                Edge e = new Edge(i,j,graph[i][j]);
                queue.offer(e);
            }
        }
        
        int[] parents = new int[graph.length];
        int[] rank = new int[graph.length];
        for(int i = 0; i<parents.length; i++){
            parents[i]=i;
            rank[i] = 1;
        }
        
        while(mst.size() < graph.length-1){
            
            Edge minEdge = queue.poll();
            if(minEdge.weight == INFINIT){
                break;
            }
            
            if(join(parents, rank, minEdge.v1, minEdge.v2)){
                mst.add(minEdge);
            }
        }
        
        return parents;
    }
    
//---------------------DIJKSTRA------------------
    
    public static double[] dijkstra(double[][] graph, int start){
        double[] distances = new double[graph.length];
        ArrayDeque<Integer> pendingNodes = new ArrayDeque<Integer>();
        ArrayDeque<Double> pendingDistances = new ArrayDeque<Double>();
        boolean[] processed = new boolean[graph.length];
        
        pendingNodes.push(start);
        pendingDistances.push(0.0);
        
        while(!pendingNodes.isEmpty()){
            int node = pendingNodes.pop();
            double distance = pendingDistances.pop();
            if(processed[node] && distance >= distances[node])
                continue;
            distances[node] = distance; // Actualizar distancia
            if(!processed[node])
                processed[node] = true;
            //obtener todos los aristas que parten de node
            for(int i =0; i<graph.length; i++){
                if(graph[node][i]<INFINIT){
                    pendingNodes.push(i);
                    pendingDistances.push(graph[node][i]+distance);
                }
            }
        }
        
        return distances;
    }
    
    public static String dijkstra_path(double[][] graph, int start, int end){
        double[] distances = new double[graph.length];
        ArrayDeque<Integer> pendingNodes = new ArrayDeque<Integer>();
        ArrayDeque<Double> pendingDistances = new ArrayDeque<Double>();
        ArrayDeque<Integer> pendingParents = new ArrayDeque<Integer>();
        boolean[] processed = new boolean[graph.length];
        int[] path = new int[graph.length];
        
        pendingNodes.push(start);
        pendingDistances.push(0.0);
        pendingParents.push(start);
        
        while(!pendingNodes.isEmpty()){
            int node = pendingNodes.pop();
            double distance = pendingDistances.pop();
            int parent = pendingParents.pop();
            if(processed[node] && distance >= distances[node])
                continue;
            distances[node] = distance; // Actualizar distancia
            path[node] = parent;
            if(!processed[node])
                processed[node] = true;
            //obtener todos los aristas que parten de node
            for(int i =0; i<graph.length; i++){
                if(graph[node][i]<INFINIT){
                    pendingNodes.push(i);
                    pendingDistances.push(graph[node][i]+distance);
                    pendingParents.push(node);
                }
            }
        }
        
        String strPath = names[end];
        int step = end;
        while(path[step] != step){
            strPath = names[path[step]] + strPath;
            step = path[step];
        }
        return strPath + " " + (int)distances[end];
    }
//--------------------- CONNECT PARTNERS ------------------
    public static String associatePartners(double[][] graph, String v1, String v2){
        String ret;
        PriorityQueue<String> partners = new PriorityQueue<String>();
        int val1 = map.get(v1);
        int val2 = map.get(v2);
        
        int[] groups = kruskal_groups(graph);
        int group1 = findLeader(groups, val1);
        int group2 = findLeader(groups, val2);
        
        if(group1!=group2){
            // Partners association is possible
            ret = "T ";
            setEdge(graph, v1, v2, 2);            //Associate partner
            groups = kruskal_groups(graph);       // Update associations
        }
        else{
            // Partners already associated
            ret = "F ";
        }
        
        int group = findLeader(groups, val2);
        for(int i = 0; i<groups.length; i++){ // Retreive association partners
            if(findLeader(groups,i)==group)
                partners.add(names[i]);
        }
        
        while(partners.size() > 0){
            ret += partners.poll();
        }
        
        return ret;
    }
    
    
//---------------------------------------
    
    static double [][] initGraph(int size, double initVal){
        double [][] graph = new double [size][size];
        for(double[] row: graph){
            Arrays.fill(row, initVal);
        }
        return graph;
    }

    static void setEdge(double[][] graph, String v1, String v2, int weight){
        int val1 = map.get(v1);
        int val2 = map.get(v2);
        graph[val1][val2]=weight;
        graph[val2][val1]=weight; //Commentando ésta linea se puede hacer un grafo dirigido
    }
    
    static void setDirectedEdge(double[][] graph, String v1, String v2, int weight){
        int val1 = map.get(v1);
        int val2 = map.get(v2);
        graph[val1][val2]=weight;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        double[][] g = initGraph(6, INFINIT);
        setEdge(g, "A", "D", 2);
        setEdge(g, "A", "E", 4);
        setEdge(g, "D", "E", 3);
        setEdge(g, "D", "C", 2);
        setEdge(g, "C", "E", 1);
        setEdge(g, "C", "F", 6);
        setEdge(g, "C", "B", 5);
        setEdge(g, "E", "F", 4);
        setEdge(g, "F", "B", 3);

        /* Evaluation data for Exam2_IV
        double[][] g = initGraph(7, INFINIT);
        setEdge(g, "A", "B", 2);
        setEdge(g, "A", "C", 2);
        setEdge(g, "B", "D", 3);
        setEdge(g, "B", "C", 3);
        setEdge(g, "C", "G", 5);
        setEdge(g, "C", "E", 4);
        setEdge(g, "D", "G", 2);
        setEdge(g, "D", "F", 6);
        setEdge(g, "E", "G", 1);
        setEdge(g, "F", "G", 3);*/

        System.out.println("Prim:");
        List<Edge> mst = mstPrim(g);
        for(Edge e: mst){
            System.out.println(e);
        }
        System.out.println();
        
        System.out.println("Kruskal:");
        List<Edge> mst2 = kruskal(g);
        for(Edge e: mst2){
            System.out.println(e);
        }
        
        
        double[][]g2 = initGraph(6, INFINIT);
        setDirectedEdge(g2, "A", "D", 2);
        setDirectedEdge(g2, "A", "E", 4);
        setDirectedEdge(g2, "D", "E", 3);
        setDirectedEdge(g2, "D", "C", 2);
        setDirectedEdge(g2, "C", "E", 1);
        setDirectedEdge(g2, "C", "F", 6);
        setDirectedEdge(g2, "C", "B", 5);
        setDirectedEdge(g2, "E", "F", 4);
        setDirectedEdge(g2, "F", "B", 3);

        System.out.println("Dijkstra:");
        double[] distances = dijkstra(g2, map.get("A"));
        for(int i =0; i<g2.length; i++)
            System.out.println(names[i]+":"+ distances[i]);
        
        System.out.println("Path A to E = " + dijkstra_path(g2, map.get("A"), map.get("E")) );
        
        double[][]g3 = initGraph(6, INFINIT);
        setEdge(g3, "A", "D", 2);
        setEdge(g3, "A", "E", 4);
        setEdge(g3, "D", "E", 3);
        //setEdge(g3, "D", "C", 2);
        //setEdge(g3, "C", "E", 1);
        setEdge(g3, "C", "F", 6);
        setEdge(g3, "C", "B", 5);
        //setEdge(g3, "E", "F", 4);
        setEdge(g3, "F", "B", 3);
        
        System.out.println("Associate Partners:");
        System.out.println("Associate A with D " + associatePartners(g3, "A", "D"));
        System.out.println("Associate D with C " + associatePartners(g3, "D", "C"));
        System.out.println("Associate E with F " + associatePartners(g3, "E", "F"));        
        System.out.println("Associate A with B " + associatePartners(g3, "A", "B"));
    }
}
