/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import iteso_ada.utils.sort;

import java.util.Scanner;

/**
 *
 * @author Administrador
 */
public class Tarea_4_2 {

    public static void SortStudents(){
        final int ORDER = 0;
        final int NAME = 1;        
        Scanner sc = new Scanner(System.in);
        
        // Get number of test cases
        int N = sc.nextInt();        
        for(int n = 0; n < N; n ++) {
            int length = sc.nextInt();
            String[][] students = new String[length][2];
            
            // Retreive students names and current order
            for(int j=0; j<length; j++){
                students[j][ORDER] = Integer.toString(j);
                students[j][NAME] = sc.next();
            }
            
            // Update students expected order
            for(int j=0; j<length; j++){
                String name = sc.next();
                for(int h=0; h<length; h++){
                    if(students[h][NAME].equals(name)){
                        students[h][ORDER] = Integer.toString(j);
                    }
                }
            }
            
            // Sort students by Order field using Extended Insertion (Sheell)
            // in order to minimize the number of changes between students
            String movements = sort.insertion2d(students, true);
            if(movements.equals(""))
                movements = "ORDENADO";
            
            System.out.println(movements);
        }
    }   

}
