/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import java.util.Arrays;
import iteso_ada.utils.arrays;

/**
 *
 * @author Administrador
 */
public class AgenteViajero {
    static int[] bestRoute;
    static int minTime;
    
    static void calculateMinTime(int graph[][], int start){
        int N = graph.length;
        minTime = Integer.MAX_VALUE;
        bestRoute = new int[N-1];

        for(int i = 0; i<N; i++){
            if(i==start) continue;
            int[] solution = new int[N-1];
            solution[0] = i;
            boolean[] visited = new boolean[N];
            visited[start] = true;
            visited[i] = true;
            processSolution(graph, start, 1, solution, visited);
        }
        
        System.out.println("Min time: " + minTime + "," + Arrays.toString(bestRoute) + ","+start);
    }
    
    static void processSolution(int graph[][], int start, int k, int[] solution, boolean[] visited){
        int N = graph.length;        
        
        if(k==N-1){
            int time = graph[start][solution[0]] + graph[solution[N-2]][start];
            for(int i = 1; i < N - 2; i++){
                time += graph[solution[i]][solution[i+1]];
            }
            if(time < minTime){
                minTime = time;
                bestRoute = solution;
            }
        }
        else {  // Solucion aun no completa
            for(int i = 0; i<N; i++){
                if(visited[i]) continue;
                int[] newSolution = solution.clone();
                newSolution[k] = i;
                boolean[] newVisited = visited.clone();
                newVisited[i] = true;
                processSolution(graph, start, k+1, newSolution, newVisited);
            }
        }
    }
    
    // Now with BackTracking
    static void FastCalculateMinTime(int graph[][], int start){
        int N = graph.length;
        minTime = Integer.MAX_VALUE;
        bestRoute = new int[N-1];

        for(int i = 0; i<N; i++){
            if(i==start) continue;
            int[] solution = new int[N-1];
            solution[0] = i;
            boolean[] visited = new boolean[N];
            visited[start] = true;
            visited[i] = true;
            fastProcessSolution(graph, start, 1, solution, visited, graph[start][i]);
        }
        
        System.out.println("Min time: " + minTime + "," + Arrays.toString(bestRoute) + ","+start);
    }
    
    public static void fastProcessSolution(int[][] graph, int start, int k, int[] solution, boolean[] visited, int currentTime){
        int N = graph.length;
        
        if(k==N-1){
            int time = currentTime + graph[solution[N-2]][start];
            if(time<minTime){
                minTime = time;
                bestRoute = solution;
            }
        }
        else{
            for(int i = 0; i<N; i++){
                if(visited[i]) continue;
                if(minTime < currentTime + graph[solution[k-1]][i]) continue;

                int[] newSolution = solution.clone();
                newSolution[k] = i;
                boolean[] newVisited = visited.clone();
                newVisited[i] = true;
                fastProcessSolution(graph, start, k+1, newSolution, newVisited, currentTime + graph[solution[k-1]][i]);                    
            }
            
        }
    }
    
    ///////////////////////////////////////////////////////
    
    public static int[][] randomGraph(int N, int max){
        int g[][] = new int[N][N];
        for(int i = 0; i<N; i++){
            for(int j=0; j<N; j++){
                if(i==j) continue;
                g[i][j] = (int)(max*Math.random());
            }
        }
        
        return g;
    }
    
    
    //**********************HILL CLIMBING**********************************

    static int getTime(int[] solution, int [][] graph, int start){
        int time = graph[start][solution[0]]+  graph[solution[solution.length-1]][start];
        for( int i =0; i<solution.length-1; i++){
            time+= graph[solution[i]][solution[i+1]];
        }
        return time;
    }
    
   static int rand(int min, int max){
      return min+ (int)((max-min + 1)* Math.random());
   }
   
   static int[] randomPath(int N, int start){
      int[] solution = new int[N-1];
      for(int i =0, s=0; i<N; i++){
         if(i==start) continue;
         solution[s++]= i;
      }
      
      //permutación aleatoria (Fisher-Yates)
      
      for(int s = 0; s < N-1; s++){
         int t = rand(s, solution.length);
         arrays.swap(solution, s, t);
      }
      
      return solution;
   }
   
   static void hillClimbing(int G, int[][]graph, int start){
      //solución aleatoria
      int [] solution = randomPath(graph.length, start);
      //evaluar la solución
      int time = getTime(solution, graph, start);
      
      for(int g = 0; g<G; g++){
         //obtener 2 indices aletorios para hacer swap
         int i1 = rand(0, solution.length-1);
         int i2 = rand(0, solution.length-1);
         while(i2==i1) 
            i2 = rand(0, solution.length-1);
         
         //hacer el swap de i1 con i2
         arrays.swap(solution, i1, i2);
         int time1 = getTime(solution, graph, start);
         if(time1< time){
            time = time1;
         }else{
            //no fue mejor me regreso al estado anterior
            arrays.swap(solution, i1, i2);
         }
         
      }
      
      System.out.println("Min time:"+ time);
      System.out.println("Best route: "+ start+ ","+
      Arrays.toString(solution)+ ", "+ start);
   }
   
   static  void randomSolution(int G, int [][] graph, int start){
      
      int[] bestSolution = randomPath(graph.length, start);
      int bestTime = getTime(bestSolution, graph, start);
      for(int g =0; g<G; g++){
         int[] solution = randomPath(graph.length, start);
         int time = getTime(solution, graph, start);
         if(time<bestTime){
            bestTime = time;
            bestSolution = solution;
         }
      }
      System.out.println("Min time:"+ bestTime);
      System.out.println("Best route: "+ start+ ","+
      Arrays.toString(bestSolution)+ ", "+ start);
      
   }
   
   
   /************************************************************************/
    
    public static void AgenteViajero_main(String[] args) {
        /*int graph[][]={{0, 4, 1, 2},
                       {3, 0, 5, 6},
                       {2, 3, 0, 2},
                       {3, 8, 2, 0}
                       };
        int[][] graph2 = randomGraph(16, 100);
        
        long ini = System.currentTimeMillis();
        FastCalculateMinTime(graph2, 0);
        long fin =  System.currentTimeMillis();
        System.out.println("Duration: " + (fin - ini));
        */
        
      int[][] g = randomGraph(20, 100);
      System.out.println("Fast calculated:");
      FastCalculateMinTime(g, 0);
      System.out.println("Hill Climbing:");
      hillClimbing(1_000_000, g, 0);
      System.out.println("Random Solution:");
      randomSolution(1_000_000, g, 0);
    }
}
