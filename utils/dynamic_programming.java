/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

/**
 *
 * @author Administrador
 */
public class dynamic_programming {
    // ------ Binomial Coeficient ------

    /* Recursive Algorithm */
    static long calls = 0;
    static long calls2 = 0;
    static long coeficientBinomial(int N, int K){
        calls++;
        if(K==0 || K==N)
            return 1;
        if(K>N)
            return 0;
        
        return coeficientBinomial(N-1, K-1)+
                coeficientBinomial(N-1, K);
    }
    
    static long dynCoefBinomial(int N, int K){
        long [][] mat = new long[N+1][K+1];
        for(int n = 0; n<=N; n++){
            for(int k = 0; k<=K; k++){
                calls2++;
                if(k==0 || k==n)
                    mat[n][k]=1;
                else if( k > n)
                    mat[n][k] = 0;
                else
                    mat[n][k] = mat[n-1][k-1] + mat[n-1][k];
            }                
        }
        
        return mat[N][K];
    }
    
    static int coinChange(int change, int[] M){
        int[][] mat = new int[M.length + 1][change+1];
        for(int m=0; m<M.length; m++){
            for(int c=1; c<change+1; c++){
                if(m==0)
                    mat[m][c] = c; // Asumimos queesta ordenado.
                else if(c==M[m])
                    mat[m][c] = 1; // Cambio igual que la moneda
                else if(c<M[m])    // cambio menor que la moneda
                    mat[m][c]=mat[m-1][c];
                else{
                    int nuevo = mat[m][c-M[m]]+1;
                    if(nuevo < mat[m-1][c])
                        mat[m][c] = nuevo;
                    else
                        mat[m][c] = mat[m-1][c];
                }
            }
        }
        
        System.out.println("Monedas utilizadas");
        int m = M.length-1; // Ultima moneda
        int c = change;
        while(m>0){
            int cont = 0;
            while(c>0 && mat[m][c]<mat[m-1][c]){
                cont++;
                c-=M[m];                
            }
            System.out.println("De $"+M[m]+":"+cont);
            m--;
            
            if(m==0)
                System.out.println("De $"+M[m]+":"+cont);
        }
        
        return mat[M.length-1][change];
    }
    
    public static void main(String[] args) { 
        System.out.println("Coeficiente binomial 30, 15: " + coeficientBinomial(30,15) + " --- Calls: " + calls);
        System.out.println("Coeficiente binomial 30, 15: " + dynCoefBinomial(30,15) + " --- Calls: " + calls2);
        
        int[] monedas = {1,4,6};
        int change = 9;
        System.out.println("Cambio de  " + change + ": " + coinChange(change, monedas) +"Monedas");
    }
}
