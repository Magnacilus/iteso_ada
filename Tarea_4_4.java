/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import java.util.Scanner;

/**
 *
 * @author Administrador
 */
public class Tarea_4_4 {
    public static void PlayWithPlanets(){     
        Scanner sc = new Scanner(System.in);
        
        // Get number of test cases
        int N = sc.nextInt();
        
        for(int n = 0; n < N; n ++) {            
            // Get planets cicles
            long cycleA = sc.nextLong();
            long cycleB = sc.nextLong();
            
            long gcd = GratestCommonDivisor(cycleA, cycleB);
            
            long matchPoint = cycleA/gcd * cycleB;
            
            System.out.println(matchPoint);
        }
    }
    
    private static long GratestCommonDivisor(long A, long B){
        if((A%B)>0)
            B = GratestCommonDivisor(B, A%B);
        return B;
    }
}
