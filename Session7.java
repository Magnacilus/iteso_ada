/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import iteso_ada.utils.DigitalSearchTree;
import iteso_ada.utils.HashSearch;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrador
 */
public class Session7 {
    public static void session7_main(String[] args) {
        int N = 1_000_000;
        //int[] arr = arrays.createSortedIntArray(N, 5);
        //int[] arr = arrays.createIntArray(N, 0, 2*N);
        int[] arr = {1,2,3,4,5,6,7,8,9,10};
        //int[] arr = {2, 16, 16, 16, 11, 12, 14, 14, 15, 17};
        //int[] performance;
        
        try {
            //DigitalSearchTree.NodeDT root = DigitalSearchTree.createDT(arr);
            //DigitalSearchTree.printDT(root, "");
            //System.out.println("Index: " + DigitalSearchTree.searchDT(root, 5, 0));
            
            HashSearch.main(args);
        } catch (Exception ex) {
            Logger.getLogger(Session7.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
