/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import java.util.ArrayDeque;
import java.util.HashMap;

/**
 *
 * @author Administrador
 */
public class Examen_2_III {
    public static boolean DFS(int[][] graph, int start, int end, int weight){
        ArrayDeque<Integer> pendingNodes = new ArrayDeque<Integer>();
        boolean[] processed = new boolean[graph.length];
        
        pendingNodes.push(start);
        
        while(!pendingNodes.isEmpty()){
            
            int node = pendingNodes.pop();
            
            if(node == end){
                return true;
            }
            
            if(!processed[node]){                
                processed[node] = true;
                for(int i = 0; i < graph[node].length; i++){
                    if(graph[node][i] >= weight && !processed[i]){
                        pendingNodes.push(i);
                    }
                }
            }
        }
        
        return false;
    }
    
    final static int HASH_LIST_SIZE = 500;
    public static int hashCode(int[] codeValues){
        int h = codeValues[0];
        for(int i = 1; i<codeValues.length; i++){
            h = (h*9 + codeValues[i]) % HASH_LIST_SIZE;
        }
        return h;
    }
    
    public static void main(String[] args) {       
        int[][] graph = {{  0, 20, 15,  0 },
                         { 20,  0,  0, 10 },
                         { 15,  0,  0, 25 },
                         {  0, 10, 25,  0 },
        };
        int start = 0;
        int stop = 3;
        int persons = 12;

        System.out.println("DFS Search: " + DFS(graph, start, stop, persons));
        
        int[] codeValues = {7,4,8,3,0};
        int h = hashCode(codeValues);
        System.out.println("Hascode: " + h);
    }
}
