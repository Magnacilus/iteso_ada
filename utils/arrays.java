/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Administrador
 */
public class arrays {
    
    // Creates an array of size N with random values in range of min and max (inclusive)
    public static int[] createIntArray(int N, int min, int max){
        int[] array = new int[N];
        Random r = new Random();

        for(int i = 0; i< array.length; i++){
            array[i]=min + r.nextInt(max-min+1);
        }

        return array;
    }
    
        // Creates a sorted array of size N with random values in range of min.
    public static int[] createSortedIntArray(int N, int min){
        int[] array = new int[N];
        Random r = new Random();

        array[0] = r.nextInt(min);    
        for(int i = 1; i< array.length; i++){
            array[i]= array[i-1] + r.nextInt(min);
        }

        return array;
    } 
    
    // Prints in console the input array
    public static void printArray(int[] array){
        System.out.println(Arrays.toString(array));
    }
    
    // Swaps array's elements between indexes index1 and index2
    public static void swap(int[] array, int index1, int index2){
        if(index1 >= 0 && index1<array.length &&
           index2 >= 0 && index2<array.length &&
           index1 != index2){
            int temp = array[index2];
            array[index2]=array[index1];
            array[index1] = temp;
        }
    }
    
    /*
    * Checks if arr is sroted
    * param: arr: Array to check
    * asc: If TRUE check for ascending order, otherwise for Descending order.
    */
    public static boolean isSort(int[] arr, boolean asc){
        if(arr.length > 0){
            for(int i = 1; i < arr.length; i++){
                if(asc){
                    if(arr[i-1] > arr [i] )
                        return false;
                }
                else{
                    if(arr[i-1] < arr [i] )
                        return false;
                }
            }
            return true;
        }
        
        return false;
    }
    
    /*
    * Returns the maximum and minimum values in an array
    * param: arr: Array to check
    * return: {MinimumValue, MaximumValue}
    */
    public static int[] getEdgeValues(int[] arr){
        int[] ret = {0,0};
        
        if(arr.length > 0){
            int maxValue = arr[0];
            int minValue = arr[0];
            for(int i = 1; i < arr.length; i++){
                if (maxValue < arr[i])
                    maxValue = arr[i];
                if (minValue > arr[i])
                    minValue = arr[i];
            }
            
            ret[0] = minValue;
            ret[1] = maxValue;
        }
        
        return ret;
    }
    
    /*
    * Returns the maximum value in an array
    * param: arr: Array to check
    */
    public static int getMaxValue(int[] arr){
        return getEdgeValues(arr)[1];
    }
    
    /*
    * Returns the minimum value in an array
    * param: arr: Array to check
    */
    public static int getMinValue(int[] arr){
        return getEdgeValues(arr)[0];
    }
}
