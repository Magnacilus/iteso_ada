/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

/**
 * ADA - Examen 2: Tower Excercises I, II
 * @author Fernando Rodríguez
 */
public class Examen_2_I_II {   
    final static int MAX_BLOCKS = 30;
    final static int MAX_WEIGHT = 100;
    final static int MAX_CAPACITY = 300;
    
    static class Block implements Comparable<Block>{
        int index = 0;
        int weight =  0;
        int capacity = 0;
        
        public Block(int index, int weight, int capacity){
            this.index = index;
            this.weight = weight;
            this.capacity = capacity;
        }
        
        public int getIndex() { return index; }
        
        public int LoadCapacity(){
            return capacity - weight;
        }               

        @Override
        public int compareTo(Block block){
            if(this.LoadCapacity() > block.LoadCapacity())
                return -1;
            if(this.LoadCapacity() < block.LoadCapacity())
                return 1;
            return 0;
        }
    }
    
    public static int[] obtenerTorreGreedy(int[] weights, int[] capacities){
        List<Block> blocks = new ArrayList<Block>();
        List<Block> tower = new ArrayList<Block>();
        
        for(int i = 0; i < weights.length; i++){
            blocks.add(new Block(i, weights[i], capacities[i]));
        }
        
        Collections.sort(blocks);
        
        int capacity = MAX_CAPACITY;
        for(Block block : blocks){
            if(block.weight <= capacity){
                tower.add(block);
                if(capacity == MAX_CAPACITY)
                    capacity = block.LoadCapacity();
                else
                    capacity -= block.weight;
            }
        }
        
        int[] towerBlocks = new int[tower.size()];
        int i = 0;
        for(Block block : tower)
            towerBlocks[i++] = block.getIndex();
        
        return towerBlocks;
    }
        
    public static int maximaAlturaBT(int[] weights, int[] capacities){   
        List<Integer> levels = new ArrayList<Integer>();
        int[] flags = new int[weights.length];
        levels = addLevel(weights, capacities, flags, levels, MAX_CAPACITY);
        return levels.size();
    }   
    
    public static List<Integer> addLevel(int[] weights, int[] capacities, int[] flags, List<Integer> levels, int capacity){        
        List<Integer> previousLevels = new ArrayList<Integer>(levels);        
        for(int i = 0; i < flags.length; i++){
            if(flags[i] == 0){
                int[] newFlags = flags.clone();
                newFlags[i] = 1;
                int currentCapacity = (capacity == MAX_CAPACITY) ? capacities[i] : capacity;
                if(currentCapacity >= weights[i]){
                    List<Integer> newLevels = new ArrayList<Integer>(levels);
                    newLevels.add(i);
                    int newCapacity = currentCapacity - weights[i];
                    if(newCapacity > capacities[i]) currentCapacity = capacities[i] - weights[i];
                    newLevels = addLevel(weights, capacities, newFlags, newLevels, currentCapacity - weights[i]);
                    if(newLevels.size()>previousLevels.size()){
                        previousLevels = newLevels;
                    }
                }
            }
        }
        
        return previousLevels;        
    }
    
    public static void main(String[] args) {       
        /* Dynamic Input */
        /* Number of blocks, Blocks Weights, Blocks Capacities*/
//        Scanner sc = new Scanner(System.in);        
//
//        int blocks = sc.nextInt();
//        weights = new int[blocks];
//        capacities = new int[blocks];
//
//        for(int i = 0; i < blocks; i++){
//            weights[i] = sc.nextInt();
//        }
//        for(int i = 0; i < blocks; i++){
//            capacities[i] = sc.nextInt();
//        }

        /* Static Input */
        int[] weights = new int[]{1,5,3};
        int[] capacities = new int[]{7, 10, 4};
        
        int[] tower = obtenerTorreGreedy(weights, capacities);        
        String strTower = "";
        for(int i = 0; i < tower.length; i++){
            if(strTower.length() > 0 ) strTower+= ",";
            strTower += Integer.toString(tower[i]);
        }
        System.out.println("Tower (Greedy): {" + strTower + "}");
        
        int maxHigh = maximaAlturaBT(weights, capacities);
        System.out.println("Máximum High (Back tracking): " + maxHigh);
        
    }
}
