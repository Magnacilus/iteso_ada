/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;
import static iteso_ada.utils.GraphStreamExamples.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import static org.graphstream.algorithm.Toolkit.randomNode;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.Path;
import org.graphstream.graph.implementations.MultiGraph;
import static org.graphstream.ui.graphicGraph.GraphPosLengthUtils.nodePosition;
import org.graphstream.ui.spriteManager.Sprite;
import org.graphstream.ui.spriteManager.SpriteManager;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;
import org.graphstream.algorithm.APSP;
import org.graphstream.algorithm.APSP.APSPInfo;
import org.graphstream.algorithm.APSP.Progress;
import static org.graphstream.algorithm.Toolkit.randomNode;
import static org.graphstream.ui.graphicGraph.GraphPosLengthUtils.nodePosition;


/**
 *
 * @author Administrador
 */
public class TrainYard {
    public static TrainYard train_yard;
    
    public static class Movement{
        public String carId, startId, targetId;
        public Path path;
        public Movement(String carId, String startId, String targetId){
            this.carId = carId;
            this.startId = startId;
            this.targetId = targetId;
        }
    }
    
    public TrainYard(){
        
    }
    
    private String strNodeId(int x, int y){
        return String.format("TN_%d_%d", x, y);
    }
    
    private String strNode(int x, int y){
        String nodeId = String.format("\"%s\"", strNodeId(x, y));
        String node = String.format("an %s\n", nodeId);
        node += String.format("cn %s  \"ui.label\":%s\n", nodeId, nodeId);
        node += String.format("cn %s  \"z_level\":0\n", nodeId);
        node += String.format("cn %s  \"xyz\":%d,%d,%d\n", nodeId, x, y, 0); // Z = 0
        return node;
    }
    
    private String strEdgeId(String nodeA, String nodeB){
        return String.format("TE_%s_%s", nodeA, nodeB);
    }
    
    private String strEdge(String nodeA, String nodeB){
        String edgeId = String.format("\"%s\"", strEdgeId(nodeA, nodeB));
        //String edge = String.format("ae %s \"%s\" > \"%s\"\n", edgeId, nodeA, nodeB); // Edge with direction
        String edge = String.format("ae %s \"%s\"  \"%s\"\n", edgeId, nodeA, nodeB);
        edge += String.format("ce %s  \"name\":\" \"\n", edgeId);
        edge += String.format("ce %s  \"length\":%d\n", edgeId, 1);        
        return edge;
    }
    
    public boolean build_yard(String name, int tracks, int maxTrackLength, int edge){
        boolean ret = false;
        
        PrintWriter writer = null;
        
        try {
            writer = new PrintWriter(name + ".dgs", "UTF-8");           
        }
        catch (IOException  ex) {
            System.out.println("Exception: " + ex);
            if(writer != null){
                writer.close();
            }
        }
        
        if(writer != null){
            writer.println("DGS004");
            writer.println("\"Rail Yard A\" 0 0");
            writer.println("cg  \"ui.multigraph\":\"true\"0");
        
            for(int x = 0; x < maxTrackLength + edge * 2; x++){
                for(int y = 0; y < tracks; y++){
                    /* Check if Track nodes should be set */
                    if ((y == 0) ||
                        (y == x - edge) || 
                        ((y < maxTrackLength + edge - x) && (x - edge > y))){
                        writer.println(strNode(x,y));
                    }
                }
            }
            
            for(int x = 0; x < maxTrackLength + edge * 2; x++){
                for(int y = 0; y < tracks; y++){
                    if(x > 0){
                        /* Check if Track nodes should be set */
                        if (y == 0){
                            writer.println(strEdge(strNodeId(x-1,y), strNodeId(x,y)));
                        }
                        else if ((y == x - edge))
                        {
                            writer.println(strEdge(strNodeId(x-1,y-1), strNodeId(x,y)));
                        }
                        else if ((y < maxTrackLength + edge - x) && (y < x - edge))
                        {
                            writer.println(strEdge(strNodeId(x-1,y), strNodeId(x,y)));
                        }
                        else if ((y == maxTrackLength + edge - x) && (y < x - edge))
                        {
                           writer.println(strEdge(strNodeId(x-1,y), strNodeId(x,y-1)));
                        }
                    }
                }
            }
            
            writer.close();
            ret = true;
        }
        
        return ret;
    }
            
    private Graph load_yard(String strFileName){
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        Graph graph = new MultiGraph("TrainYard");
		
        try {
                graph.read(strFileName + ".dgs");
        } catch(Exception e) {
                e.printStackTrace();
                System.exit(1);
        }       
        
        return graph;
    }
    
    private void DisplayGraph(Graph graph, String strStyleFile){
        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");

        String styleSheet = FileReader(strStyleFile + ".css");
        graph.addAttribute("ui.stylesheet", styleSheet);
        Viewer viewer = graph.display(false);
        ViewPanel view = viewer.getDefaultView();
        
        view.resizeFrame(800, 600);
    }
    
    public static void TestA(){
        String yardName = "train_yard_a";
        //train_yard.build_yard(yardName, 3, 10, 2);
        
        List<Movement> movements = new ArrayList<Movement>();
        movements.add(new Movement("A","TN_5_2", "TN_6_0"));
        movements.add(new Movement("B","TN_6_2", "TN_5_0"));
        movements.add(new Movement("C","TN_7_2", "TN_5_1"));
        
        Test(yardName, movements, 250);
    }
    
    public static void TestB(){
        String yardName = "train_yard_b";
        //train_yard.build_yard(yardName, 9, 40, 2);
        
        List<Movement> movements = new ArrayList<Movement>();
        movements.add(new Movement("UP 8333","TN_21_3", "TN_28_0"));
        movements.add(new Movement("UP 7965","TN_20_3", "TN_31_2"));
        movements.add(new Movement("UP 6548","TN_19_3", "TN_32_2"));
        movements.add(new Movement("FXE 1001","TN_18_3", "TN_10_1"));
        movements.add(new Movement("UP 6025","TN_17_3", "TN_9_1"));
        movements.add(new Movement("FSRR 1005","TN_16_3", "TN_8_1"));
        movements.add(new Movement("KCSM 1322","TN_15_3", "TN_7_1"));
        movements.add(new Movement("KCSM 8433","TN_14_3", "TN_33_0"));
        movements.add(new Movement("FSRR 5321","TN_13_3", "TN_32_0"));
        movements.add(new Movement("FXE 1120","TN_12_3", "TN_31_0"));
        movements.add(new Movement("FSRR 3223","TN_11_3", "TN_30_0"));
        movements.add(new Movement("FSRR 1421","TN_9_3", "TN_35_0"));
        movements.add(new Movement("FSRR 1825","TN_10_3", "TN_34_0"));
        
        movements.add(new Movement("FSRR 1115","TN_10_5", "TN_27_0"));
        movements.add(new Movement("FXE 1421","TN_11_5", "TN_18_8"));
        movements.add(new Movement("FXE 1122","TN_12_5", "TN_17_8"));
        movements.add(new Movement("FXE 1342","TN_13_5", "TN_16_8"));
        movements.add(new Movement("FXE 3325","TN_14_5", "TN_15_8"));
        movements.add(new Movement("FXE 3325","TN_15_5", "TN_8_0"));
        movements.add(new Movement("CIA 4569","TN_16_5", "TN_14_8"));
        movements.add(new Movement("CIA 6987","TN_17_5", "TN_7_0"));
        movements.add(new Movement("CIA 4521","TN_18_5", "TN_13_8"));
        movements.add(new Movement("UP 8479","TN_19_5", "TN_33_2"));
        
        Test(yardName, movements, 25);
    }
    
    public static void Test(String yardName, List<Movement> movements, long delay){
        /* Prepare the Train Yard */                
        Graph graph = train_yard.load_yard(yardName);
        
        /* Set the Cars */        
        SpriteManager sman = new SpriteManager(graph);
        for(Movement mov: movements){
            Sprite car = sman.addSprite(mov.carId);
            Node n = graph.getNode(mov.startId);
            double p[] = nodePosition(n);
            car.setPosition(p[0], p[1], p[2]);
            car.setAttribute("ui.label", mov.carId);
            
            Sprite target = sman.addSprite("T:" + mov.carId);
            Node nTarget = graph.getNode(mov.targetId);
            double pTarget[] = nodePosition(nTarget);
            target.setPosition(pTarget[0], pTarget[1], pTarget[2]);
            target.setAttribute("ui.label", mov.carId);
            target.addAttribute("ui.class", "target");
        }
        
        train_yard.DisplayGraph(graph, yardName);
        
        /* Calculate shorted paths with All Pair Shorted Path (APSP) algorithm */
        APSP apsp = new APSP();
        apsp.init(graph); // registering apsp as a sink for the graph
        apsp.setDirected(false); // undirected graph
        apsp.setWeightAttributeName("length"); // Seting the "length" property as the edege weigth

        apsp.compute(); // the method that actually computes shortest paths

        for(Movement mov: movements){
            APSPInfo info = graph.getNode(mov.startId).getAttribute(APSPInfo.ATTRIBUTE_NAME);
            mov.path = info.getShortestPathTo(mov.targetId);
            System.out.println(mov.path);
            for(Node n: mov.path.getNodePath()){
                Sprite car = sman.getSprite(mov.carId);
                double p[] = nodePosition(n);
                car.setPosition(p[0], p[1], p[2]);
                car.setAttribute("ui.label", mov.carId);
                try {
                    Thread.sleep(delay);
                }
                catch (Exception  ex) {
                    System.out.println("Exception: " + ex);
                }
            }                
        }
    }
            
    public static void main(String[] args) throws IOException {
        //GraphStreamExamples.Example3();
        
        train_yard = new TrainYard();        
        TestA();
        TestB();
    }
}
