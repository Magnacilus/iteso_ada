/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class sort {

/*-----------------------------------------------------------------------------

    Session2:   Selection, Insertion and Bubble Algorithms

-----------------------------------------------------------------------------*/
    
    /*
    * Sorts and array by Selection Algorithm
    * arr: Array to be sorted.
    * boAsc: If true applies descending sorting, else applies ascending sorting.
    */
    public static int[] selection(int[] arr, boolean boAsc){
        int[] ret = {0,0};
        int comparisons = 0;
        int movements = 0;
        for(int i = 0; i < arr.length-1; i++){
            int minIndex = i;
            for(int j = i+1; j < arr.length; j++){
                comparisons++;
                if(boAsc){                    
                    if(arr[j] < arr[minIndex]){
                        minIndex = j;
                    }
                }
                else {
                    if(arr[j] > arr[minIndex]){
                        minIndex = j;
                    }
                }
            }
            
            if(minIndex != i){
                movements+= 3;  // Swap operation takes mainly 3 movements
                arrays.swap(arr, i, minIndex);
            }
        }
        
        ret[0] = comparisons;
        ret[1] = movements;
        return ret;
    }
    
    /**
     * Applying Selection algorithm sorts a two dimensional array of form [N][2]
     * Where frist column [X][0] indicates the item expected order
     * and secodn colomn  [X][1] indicates the item name.
     * @param arr: Two dimensional array to be sorted.
     * @param boAsc: If true apply ascending sorting otherwise desending
     * @return a string witht the item names that were moved to an earlier position.
     */
    public static String selection2d(String[][] arr, boolean boAsc){
        String ret = "";
        for(int i = 0; i < arr.length-1; i++){
            int minIndex = i;
            for(int j = i+1; j < arr.length; j++){
                if(boAsc){   
                    if(arr[j][0].compareTo(arr[minIndex][0]) < 0){
                        minIndex = j;
                    }
                }
                else {
                    if(arr[j][0].compareTo(arr[minIndex][0]) > 0){
                        minIndex = j;
                    }
                }
            }
            
            if(minIndex != i){
                String[] data = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = data;
                ret = ret + arr[i][1] + " ";
            }
        }
        
        if(ret.endsWith(" "))
            ret = ret.substring(0, ret.length()-1);
        
        return ret;
    }

    /*
    * Sorts and array by Insertion Algorithm
    * arr: Array to be sorted.
    * boAsc: If true applies descending sorting, else applies ascending sorting.
    */
    public static int[] insertion(int[] arr, boolean boAsc){
        int[] ret = {0,0};
        int comparisons = 0;
        int movements = 0;
        
        for(int i = 1; i < arr.length; i++){
            int value = arr[i];
            int j = i -1;
            
            if(boAsc){
                while(j >= 0 && value < arr[j]){
                    comparisons++;
                    movements++;
                    arr[j+1] = arr[j];
                    j--;
                }
            }
            else
            {
                while(j >= 0 && value > arr[j]){
                    comparisons++;
                    movements++;
                    arr[j+1] = arr[j];
                    j--;
                }
            }
            
            if(j != (i - 1))
            {
                // Some minor items moved, then insert value. 
                movements++;
                arr[j+1] = value;
            }
            else
            {
                // No minor items moved.
                comparisons++;
            }
        }
        
        ret[0] = comparisons;
        ret[1] = movements;
        return ret;
    }
    
    /**
     * Applying Insertion algorithm to sort a two dimensional array of form [N][2]
     * Where frist column [X][0] indicates the item expected order
     * and secodn colomn  [X][1] indicates the item name.
     * @param arr: Two dimensional array to be sorted.
     * @param boAsc: If true apply ascending sorting otherwise desending
     * @return a string witht the item names that were moved to an earlier position.
     */
    public static String insertion2d(String[][] arr, boolean boAsc){
        String movements = "";
        
        for(int i = 1; i < arr.length; i++){
            String[] value = arr[i];
            int j = i -1;
            
            if(boAsc){
                while(j >= 0 && (Integer.parseInt(value[0]) < Integer.parseInt(arr[j][0]))){
                    arr[j+1] = arr[j];
                    j--;
                }
            }
            else
            {
                while(j >= 0 && (Integer.parseInt(value[0]) > Integer.parseInt(arr[j][0]))){
                    arr[j+1] = arr[j];
                    j--;
                }
            }
            
            if(j != (i - 1))
            {
                // Some minor items moved, then insert value.
                movements += value[1] + " ";
                arr[j+1] = value;
            }
            else
            {
                // No minor items moved.
            }
        }
        
        if(movements.endsWith(" "))
            movements = movements.substring(0, movements.length()-1);
        
        return movements;
    }   
    
    /*
    * Sorts and array by Bubble Algorithm
    * arr: Array to be sorted.
    * boAsc: If true applies descending sorting, else applies ascending sorting.
    */    
    public static int[] bubble(int[] arr, boolean boAsc){
        int[] ret = {0,0};
        int comparisons = 0;
        int movements = 0;
        
        boolean swapped = true;
        
        for(int i = 0; i < arr.length - 1 && swapped; i++){
            swapped = false;
            for( int j = 0; j < arr.length - i - 1; j++ ){
                comparisons++;
                if(boAsc){
                    if(arr[j] > arr[j+1]){
                        movements++;
                        arrays.swap(arr, j, j+1);
                        swapped = true;
                    }
                }
                else{
                    if(arr[j] < arr[j+1]){
                        movements++;
                        arrays.swap(arr, j, j+1);
                        swapped = true;
                    }
                }
            }
        }
        
        ret[0] = comparisons;
        ret[1] = movements;
        return ret;
    }

/*-----------------------------------------------------------------------------

    Homework2:   Shell Algorithm

-----------------------------------------------------------------------------*/

    /*
    * Gets the Start Gap based on: k=3k+1 for Sheel Algorithm
    * arrLength: Array length.
    */         
    private static int getStartGap(int arrLength){
        int ret = 0;
        for(int k = 0; k < arrLength; k = 3 * k + 1){
            ret = k;
        }
        
        return ret;
    }    
    
    /*
    * Gets the next lower Gap based on: K = 3k - 1 >> k = (K-1)/3
    * for Sheel Algorithm
    * currentGap: Reference Gap.
    */   
    private static int getLowerGap(int currentGap){
        return (currentGap - 1)/3;
    }
    
    /*
    * Sorts and array by Sheel Algorithm
    * arr: Array to be sorted.
    * boAsc: If true applies descending sorting, else applies ascending sorting.
    */     
    public static int[] shell(int[] arr, boolean boAsc){
        int[] ret = {0,0};
        int comparisons = 0;
        int movements = 0;
        
        int k = getStartGap(arr.length);
        
        while(k > 0){
            for(int h = 0; h < k; h++){
                for(int i = k + h; i < arr.length; i += k){
                    int value = arr[i];
                    int j = i - k;

                    if(boAsc){
                        while(j >= 0 && value < arr[j]){
                            comparisons++;
                            movements++;
                            arr[j+k] = arr[j];
                            j -= k;
                        }
                    }
                    else
                    {
                        while(j >= 0 && value > arr[j]){
                            comparisons++;
                            movements++;
                            arr[j+k] = arr[j];
                            j -= k;
                        }
                    }

                    if(j != (i - k))
                    {
                        // Some minor items moved, then insert value. 
                        movements++;
                        arr[j+k] = value;
                    }
                    else
                    {
                        // No minor items moved.
                        comparisons++;
                    }
                }
            }
            
            k = getLowerGap(k);
        }
        
        ret[0] = comparisons;
        ret[1] = movements;
        return ret;
    }
    
    /*
    * Adaptation of Shell algorithm to suppor two dimensional array
    * arr: Two diemsnional array to be sorted.
    * field: Column to be the reference of the sorting criteria
    * boAsc: If true applies descending sorting, else applies ascending sorting.
    */  
    public static int[] shell2d(String[][] arr, int field, boolean boAsc){
        int[] ret = {0,0};
        int comparisons = 0;
        int movements = 0;
        
        int k = getStartGap(arr.length);
        
        while(k > 0){
            for(int h = 0; h < k; h++){
                for(int i = k + h; i < arr.length; i += k){
                    String[] value = arr[i];
                    int j = i - k;

                    if(boAsc){
                        while(j >= 0 && value[field].compareTo(arr[j][field]) < 0){
                            comparisons++;
                            movements++;
                            arr[j+k] = arr[j];
                            j -= k;
                        }
                    }
                    else
                    {
                        while(j >= 0 && value[field].compareTo(arr[j][field]) > 0){
                            comparisons++;
                            movements++;
                            arr[j+k] = arr[j];
                            j -= k;
                        }
                    }

                    if(j != (i - k))
                    {
                        // Some minor items moved, then insert value. 
                        movements++;
                        arr[j+k] = value;
                    }
                    else
                    {
                        // No minor items moved.
                        comparisons++;
                    }
                }
            }
            
            k = getLowerGap(k);
        }
        
        ret[0] = comparisons;
        ret[1] = movements;
        return ret;
    }
    
    /**
     * Applying Sheel algorithm to sort a two dimensional array of form [N][2]
     * Where frist column [X][0] indicates the item expected order
     * and secodn colomn  [X][1] indicates the item name.
     * @param arr: Two dimensional array to be sorted.
     * @param boAsc: If true apply ascending sorting otherwise desending
     * @return a string witht the item names that were moved to an earlier position.
     */
    public static String shell2d(String[][] arr, boolean boAsc){
        String movements = "";
        
        int k = getStartGap(arr.length);
        
        while(k > 0){
            for(int h = 0; h < k; h++){
                for(int i = k + h; i < arr.length; i += k){
                    String[] value = arr[i];
                    int j = i - k;

                    if(boAsc){
                        while(j >= 0 && (Integer.parseInt(value[0]) < Integer.parseInt(arr[j][0]))){
                            //movements += arr[j][1];
                            arr[j+k] = arr[j];
                            j -= k;
                        }
                    }
                    else
                    {
                        while(j >= 0 && (Integer.parseInt(value[0]) > Integer.parseInt(arr[j][0]))){
                            //movements += arr[j][1];
                            arr[j+k] = arr[j];
                            j -= k;
                        }
                    }

                    if(j != (i - k))
                    {
                        // Some minor items moved, then insert value. 
                        movements += value[1] + " ";
                        arr[j+k] = value;
                    }
                }
            }
            
            k = getLowerGap(k);
        }
        
        if(movements.endsWith(" "))
            movements = movements.substring(0, movements.length()-1);
        
        return movements;
    }

/*-----------------------------------------------------------------------------

    Session3:   Heap Sort Algorithm

-----------------------------------------------------------------------------*/

    private static int iGetParent(int index){
        return (index-1)/2;
    }
    
    private static int iGetLeftChild(int index){
        return (2 * index) + 1;
    }
    
    private static int iGetRigthChild(int index){
        return 2*(index + 2);
    }

    /*
    * Heap Sort algorithm implementation. Sorts an array in ascending order
    * arr: Array to be sorted.
    */     
    public static int[] heap(int[] arr){
        int[] ret = {0,0};
        int comparisons = 0;
        int swaps = 0;
        
        // Paso1: Crear el montículo
        for(int i = 1; i < arr.length; i++){
            int child = i;
            int parent = iGetParent(child);
            while(child > 0 && arr[parent]<arr[child]){
                arrays.swap(arr, child, parent);
                swaps++;
                child = parent;
                parent = iGetParent(child);
            }
        }
        
        // Paso2: Push-down
        int lastPosition = arr.length-1;
        
        for(int i = 0; i < arr.length-1; i++, lastPosition--){
            comparisons++;
            arrays.swap(arr, 0, lastPosition);
            int parent = 0;
            int leftChild = 1;
            int rigthChild = lastPosition > 2 ? 2: 1;
            int maxChild = arr[leftChild] > arr[rigthChild] ? leftChild:rigthChild;
            comparisons++;
            while(arr[parent] < arr[maxChild] && maxChild < lastPosition){
                comparisons++;
                swaps++;
                arrays.swap(arr, parent, maxChild);
                parent = maxChild;
                leftChild = iGetLeftChild(parent);
                if(leftChild >= lastPosition) {
                    comparisons++;
                    break;
                }
                rigthChild = iGetRigthChild(parent);
                if(rigthChild >= lastPosition){
                    comparisons++;
                    rigthChild = leftChild;
                }
                
                maxChild = arr[leftChild] > arr[rigthChild] ? leftChild:rigthChild;
            }
        }
        
        ret[0] = comparisons;
        ret[1] = swaps;
        return ret;
    }
    
/*-----------------------------------------------------------------------------

    Session4:   Quick Sort Algorithm initial implemenation
    Homework3:  Finish Quick Sort implementation

-----------------------------------------------------------------------------*/
    /*
    * Generates an array partition based on QuickSort algoritm
    * arr: Array to partitionate
    * left: Sub Array left index
    * right: Sub Array right index
    * performance: keeps track of number of comparisons and swaps
    */
    private static int partition(int[] arr, int left, int right, int[] performance){
        final int COMPARISONS = 0;
        final int SWAPS = 1;
        int pivot = arr[right];
        int p1 = left - 1;
        int p2 = right; // Pivot is at the end, discarding it.
        while (p1 < p2){
            performance[COMPARISONS]++;
            p1++;
            p2--;
            while( p1 <= right && arr[p1] < pivot){
                performance[COMPARISONS]++;
                p1++;
            }
            while(p2 > left && arr[p2] > pivot){
                performance[COMPARISONS]++;
                p2--;
            }
            if(p1 < p2){
                performance[SWAPS]++;
                arrays.swap(arr, p1, p2);
            }            
        }
        performance[SWAPS]++;
        arrays.swap(arr, p1, right);
        
        return p1;
    }
    
    /*
    * Sorts an array applying QuickSort algorithm in recursive way
    * arr: Array to partitionate
    * left: Sub Array left index
    * right: Sub Array right index
    */
    private static void doQuicksort(int[] arr, int left, int right, int[] performance){
        final int COMPARISONS = 0;
        final int SWAPS = 1;
        
        performance[COMPARISONS]++;
        if(right > left){
            performance[COMPARISONS]++;
            if(right - left == 1){
                performance[COMPARISONS]++;
                if(arr[left]>arr[right]){
                    performance[SWAPS]++;
                    arrays.swap(arr, left, right);
                }
            }
            else{
                int partition = partition(arr, left, right, performance);
                performance[COMPARISONS]++;
                if(partition > left){
                    doQuicksort(arr, left, partition - 1, performance);
                }
                 performance[0]++;
                if(partition < right){
                    doQuicksort(arr, partition + 1, right, performance);
                }
            }
        }
    }
    
    /*
    * Public method to trigger QuickSort algorithm
    * arr: Array to partitionate
    */
    public static int[] quicksort(int[] arr){
        int[] performance = {0, 0};
        doQuicksort(arr, 0, arr.length - 1, performance);
        return performance;
    }
    
/*-----------------------------------------------------------------------------

    Homework3:   Radix Sort Algorithm.

-----------------------------------------------------------------------------*/
    /*
    * Radix Sort algorithm implementation. Sorts an array in ascending order
    * arr: Array to be sorted.
    */
    public static int[] radix(int[] arr){
        int[] ret = {0,0};
        int comparisons = 0;
        int swaps = 0;
        
        int maxValue = arrays.getMaxValue(arr);
        final int RADIX = 10;
        int digit = 1;
                
        // declare and initialize bucket[]
        List<Integer>[] bucket = new ArrayList[RADIX];
        for (int i = 0; i < bucket.length; i++){
            comparisons++;
            bucket[i] = new ArrayList<>();
        }

        do{
            // Acomodate items in bucket
            for(Integer i : arr){
                comparisons++;
                int temp = i / digit;
                bucket[temp % RADIX].add(i);
                swaps++;
            }

            // Extract itmes from bucket
            for(int i=0, j=0; i<bucket.length; i++){
                for(Integer value : bucket[i]){
                    comparisons++;
                    arr[j++]=value;
                    swaps++;
                }
                bucket[i].clear();
            }

            digit *= RADIX;
        }while(maxValue / digit > 0);
        
        ret[0] = comparisons;
        ret[1] = swaps;
        return ret;
    }
    
/*-----------------------------------------------------------------------------

    Homework3:   Merge Sort Algorithm.

-----------------------------------------------------------------------------*/

    /*
    * Generates an array partition based on Merge Sort algoritm
    * arrSrc: Source array containing unsort elements
    * arrTgt: Target array to contain sorted elements
    * left: Sub Array left index
    * right: Sub Array right index
    * performance: keeps track of number of comparisons and swaps
    */
    private static void doMergeSort(int[] arSrc, int[] arTgt, int left, int right, int[] performance){
        final int COMPARISONS = 0;
        final int SWAPS = 1;
        int length = right - left + 1;
        if(length > 2){
            performance[COMPARISONS]++;
            int mediumIndex = length / 2;

            // Do your recursive stuff
            int leftLength = mediumIndex;
            doMergeSort(arSrc, arTgt, left, (left + mediumIndex) -1, performance);
            int rightLength = length - mediumIndex;
            doMergeSort(arSrc, arTgt, left + mediumIndex, right, performance);
            arSrc = arTgt.clone();
            
            int leftIndx=left;
            int rightIndex=left + mediumIndex;
            for(int i = left; i < left + length; i++){
                performance[COMPARISONS]+= 2;
                if(((leftIndx - left) < leftLength) && ((rightIndex - (left + mediumIndex)) < rightLength)){
                    if (arSrc[leftIndx] < arSrc[rightIndex]){
                        performance[SWAPS]++;
                        arTgt[i] = arSrc[leftIndx++];
                    }
                    else
                    {
                        performance[SWAPS]++;
                        arTgt[i] = arSrc[rightIndex++];
                    }
                }
                else if ((leftIndx - left) < leftLength){
                    performance[SWAPS]++;
                    arTgt[i] = arSrc[leftIndx++];
                }
                else if ((rightIndex - (left + mediumIndex)) < rightLength){
                    performance[SWAPS]++;
                    arTgt[i] = arSrc[rightIndex++];
                }
                else{
                    System.out.println("MergeSort error - wrong index");
                }
            }
        }
        else if(length == 2){
            performance[0]++;
            if(arSrc[left]>arSrc[right]){
                performance[SWAPS]++;
                arrays.swap(arTgt, left, right);
            }
        }
    }
    
    /*
    * Public method to trigger MergeSort algorithm
    * arr: Array to partitionate
    */
    public static int[] mergesort(int[] arr, int[] performance){
        int[] arrTgt = arr.clone();
        doMergeSort(arr, arrTgt, 0, arr.length - 1, performance);
        return arrTgt;
    }
    
    /**
     * Reorganize array values in a sorted order by considering two virtual
     * arrays contained in the physical array but delimited by two copule of
     * indexes..
     * @param array: Array containing the two virtual arrays.
     * @param tmpArray: A temporal array used duirng the data transfer.
     * @param left1: Left index of the virtual array 1.
     * @param right1: Right index of the virtual array 1.
     * @param left2: Left index of the virtual array 2.
     * @param right2: Right index of the virtual array 2.
     */
    static void merge(int[] array, int[] tmpArray, int left1, int right1, int left2, int right2) {
        int count = right1 - left1 + right2 - left2 + 2;
        int array1Index = left1;
        int array2Index = left2;
        for(int i = 0; i < count; i ++) {
            if(array2Index > right2) {
                tmpArray[i] = array[array1Index];
                array1Index ++;
            } else if(array1Index > right1) {
                tmpArray[i] = array[array2Index];
                array2Index ++;
            } else if(array[array1Index] < array[array2Index]) {
                tmpArray[i] = array[array1Index];
                array1Index ++;
            } else {
                tmpArray[i] = array[array2Index];
                array2Index ++;
            }
        }
        for(int i = 0; i < count; i ++) array[left1 + i] = tmpArray[i]; 
    }    
    
    /**
     * Merge Sort Algorithm implemented in an iterative way
     * @param array: Contains the data to be sorted.
     */
    public static void mergeSortIte(int[] array){
        ArrayDeque<Integer> stack = new ArrayDeque<Integer>();
        int[] tmpArray = new int[array.length];
        stack.push(0);                  // Left limit
        stack.push(array.length - 1);   // Right limit
        stack.push(0);                  // Visited = false
        
        while(!stack.isEmpty()){
            int visited = stack.pop();
            int right = stack.pop();
            int left = stack.pop();
            
            if(right == left){
                continue;
            }
            
            int middle = left + (right - left)/2;
            if(visited == 0){
                // Parent
                stack.push(left);
                stack.push(right);
                stack.push(1);
                // Rigth child
                stack.push(middle + 1);
                stack.push(right);
                stack.push(0);
                // Left child
                stack.push(left);
                stack.push(middle);
                stack.push(0);
            }
            else{
                merge(array, tmpArray, left, middle, middle+1, right);
            }
        }
    }
            
}
