/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import iteso_ada.utils.sort;
import java.util.ArrayDeque;
import java.util.Scanner;
import java.util.Iterator;

/**
 *
 * @author Administrador
 */
public class Tarea_4_3 {
    static void merge(char[] array, char[] tmpArray, int left1, int right1, int left2, int right2) {
        int count = right1 - left1 + right2 - left2 + 2;
        int array1Index = left1;
        int array2Index = left2;
        for(int i = 0; i < count; i ++) {
            if(array2Index > right2) {
                tmpArray[i] = array[array1Index];
                array1Index ++;
            } else if(array1Index > right1) {
                tmpArray[i] = array[array2Index];
                array2Index ++;
            } else if(array[array1Index] < array[array2Index]) {
                tmpArray[i] = array[array1Index];
                array1Index ++;
            } else {
                tmpArray[i] = array[array2Index];
                array2Index ++;
            }
        }
        for(int i = 0; i < count; i ++) array[left1 + i] = tmpArray[i]; 
    }   
    
    public static String[] MergeStrings(String[] strings){
        int stringsToMerge = strings.length;
        String[] mergedStrings = new String[stringsToMerge / 2];
        
        for(int i = 0; i<stringsToMerge; i+=2){
            String longString = strings[i] + strings[i+1];
            char[] charArray = longString.toCharArray();
            char[] charArrayTemp = charArray.clone();
            merge(charArray, charArrayTemp, 0, strings[i].length()-1, strings[i].length(), charArray.length - 1);
            mergedStrings[i/2] = new String(charArray);
        }
        
        return mergedStrings;
    }
    
    public static void MergeWords(){     
        Scanner sc = new Scanner(System.in);
        
        // Get number of test cases
        int N = sc.nextInt();
        
        for(int n = 0; n < N; n ++) {            
            // Get number of Words
            int NW = sc.nextInt();
            String[] strings = new String[NW];
            
            // Retreive words
            for(int j=0; j<NW; j++){
                strings[j] = sc.next();
            }
            
            while(strings.length > 1){
                strings = MergeStrings(strings);
                for(String str : strings){
                    System.out.println(str);
                }
            }            
            
            System.out.println();
        }
    }   
}
