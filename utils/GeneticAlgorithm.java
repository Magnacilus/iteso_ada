/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import java.util.Arrays;

/**
 *
 * @author Administrador
 */
public class GeneticAlgorithm {
    static final int GENOTYPE_LENGTH = 12; // Expected to be even
    static final int POPULATION_SIZE = GENOTYPE_LENGTH * 20;
    static final int GNERATION = 10000;
    static final double CROSSOVER_ODDS = 0.8;
    static final double MUTATION_ODDS = 0.2;
    
    static final double MAX_VALUE = Math.pow(10, GENOTYPE_LENGTH -1);
    static final double MIN_X1 = -5;
    static final double MAX_X1 = 5;
    static final double MIN_X2 = -5;
    static final double MAX_X2 = 5;
    
    //Create the population, individuals array
    static Individual[] population;
    static double fitnessSum;
    static int bestIndividualIndex;
    
    static class Individual{
        double x1,x2;
        byte genotype[];
        double fitness, expectedValue;
        
        public Individual(boolean createArray){
            if(createArray){
                this.genotype = new byte[GENOTYPE_LENGTH];
            }            
        }
        
        public Individual clone(){
            Individual ind = new Individual(false); 
            ind.x1 = this.x1;
            ind.x2 = this.x2;
            ind.genotype = this.genotype.clone();
            return ind;
        }
        
        public void initRandom(){
            for(int i = 0; i<GENOTYPE_LENGTH; i++){
                this.genotype[i] = (byte)(Math.random()*10); //0..9
            }
            
            updatePhenotype();
        }
        
        public void updateFitness(){
            double f = ST (this.x1, this.x2);
            this.fitness = 1.0 - (100+f)/1100;
        }
        
        public void updatePhenotype(){
            // The cromosom {1,2,3,4} corresponds to 4,321
            x1 = 0;
            x2 = 0;
            double pot = 1;
            for(int i = 0; i < GENOTYPE_LENGTH/2; i++){
                x1 += this.genotype[i]*pot;
                x2 += this.genotype[i + GENOTYPE_LENGTH/2] * pot;
                pot*=10;
            }
            
            x1 /= MAX_VALUE;
            x1 = MIN_X1 + x1 * (MAX_X1 - MIN_X1);
            
            x2 /= MAX_VALUE;
            x2 = MIN_X2 + x2 * (MAX_X2 - MIN_X2);
        }
        
        @Override
        public String toString(){
            return String.format("<%.5f, %.5f> %s", this.x1, this.x2, Arrays.toString(this.genotype));
        }
    }
    
   //Styblinskiand Tang
   public static double ST(double x1, double x2){
      return   0.5*(Math.pow(x1,4) - 16*x1*x1+5*x1 + 
                    Math.pow(x2,4) - 16*x2*x2+5*x2);
   }
    
   static void runGeneticAlgorithm(){
       createPopulation();
       for(int i = 0; i < GNERATION; i++){
            calculateFitness();
            selection();
            crossover();
            mutation();           
       }
        
       calculateFitness();              
   } 
   
   public static void createPopulation(){
       population = new Individual[POPULATION_SIZE];
       for(int i = 0; i < POPULATION_SIZE; i++){
           population[i] = new Individual(true);
           population[i].initRandom();
       }
   }
   
   private static void calculateFitness(){
       fitnessSum = 0;
       bestIndividualIndex = 0;
       
       for(int i = 0; i < POPULATION_SIZE; i++){
           population[i].updateFitness();
           fitnessSum += population[i].fitness;
           if(population[i].fitness > population[bestIndividualIndex].fitness){
               bestIndividualIndex = i;
           }
       }
       
       System.out.println(population[bestIndividualIndex] + " " + 
               population[bestIndividualIndex].fitness);
   }
   
    public static void selection(){
       Individual[] tmpPopulation = new Individual[POPULATION_SIZE];
       
       //cycle to control the launch of the Ruleta :D
       for(int i = 0; i < POPULATION_SIZE; i++){
           double rnd = fitnessSum * Math.random();
           int j = 0;
           double fs = 0;
           while(fs<rnd && j < POPULATION_SIZE){
               fs += population[j].fitness;
               j++;
           }
           j--;
           tmpPopulation[i] = population[j].clone();
       }
       population = tmpPopulation;
   }
    
    public static void crossover(){
        for(int i = 0; i < POPULATION_SIZE; i+=2){
            if(i == bestIndividualIndex || i+1==bestIndividualIndex )
                continue;
            if(Math.random()>CROSSOVER_ODDS)
                continue;
            
            byte[] genotype1 = population[i].genotype;
            byte[] genotype2 = population[i+1].genotype;
            
            int id1 = (int)(Math.random() * (GENOTYPE_LENGTH - 1));
            int id2 = id1 + (int)(Math.random() * (GENOTYPE_LENGTH - id1));
            
            for(int id = id1; id <= id2; id++){
                if(Math.random() < 0.5){
                    byte tmp = genotype1[id];
                    genotype1[id] = genotype2[id];
                    genotype2[id] = tmp;                    
                }
            }
        }
    }
        
    public static void mutation(){
        for(int i = 0; i < POPULATION_SIZE; i++){
            if(i == bestIndividualIndex) 
                continue;
            if(Math.random() > MUTATION_ODDS) 
                continue;
            int id = (int)(Math.random()*GENOTYPE_LENGTH);
            byte[] genotype = population[i].genotype;
            byte b = (byte)(Math.random()*10); // 0..9
            while(b==genotype[id])
                b = (byte)(Math.random()*10); // 0..9
            genotype[id] = b;
        }
    }    
      
    public static void main(String[] args) {
        runGeneticAlgorithm();
    }
}
