/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import iteso_ada.utils.arrays;
import iteso_ada.utils.sort;

import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Administrador
 */
public class Tarea2 {
    
    public static void TestBench() {
        final int INVOCATION_FACTOR = 500;
        final int MIN_N = 1;                    // Should not be lower than 1!
        final int MAX_N = 200;
        final int MIN_ARRAY_VALUE = -100;
        final int MAX_ARRAY_VALUE = 100;
        final int TOTAL_ITERATIONS = MAX_N * INVOCATION_FACTOR;
        
        PrintWriter writer = null;
        
        try {
            writer = new PrintWriter("SortingAlgoritmsReport.csv", "UTF-8");           
        }
        catch (IOException  ex) {
            System.out.println("Exception: " + ex);
            if(writer != null){
                writer.close();
            }
        }
                
        if(writer != null){
            writer.println("N,Iterations,Avg Comp Selection,Avg Mov Selection," + 
                           "Avg Comp Insertion,Avg Mov Insertion," + 
                           "Avg Comp Bubble,Avg Mov Bubble," + 
                           "Avg Comp Shell,Avg Mov Shell");
            System.out.println("...%");
        
            for(int n = MIN_N; n <= MAX_N; n++){
                long[] perfSelection = {0,0};
                long[] perfInsertion = {0,0};
                long[] perfBubble = {0,0};
                long[] perfShell = {0,0};
                int[] performance;
                
                int j = 0;
                while(j<(INVOCATION_FACTOR * n)) {
                    int[] arr = arrays.createIntArray(n, MIN_ARRAY_VALUE, MAX_ARRAY_VALUE);

                    performance = sort.selection(arr.clone(), true);
                    perfSelection[0] += performance[0];
                    perfSelection[1] += performance[1];
                    
                    performance = sort.insertion(arr.clone(), true);
                    perfInsertion[0] += performance[0];
                    perfInsertion[1] += performance[1];
                    
                    performance = sort.bubble(arr.clone(), true);
                    perfBubble[0] += performance[0];
                    perfBubble[1] += performance[1];
                    
                    performance = sort.shell(arr.clone(), true);
                    perfShell[0] += performance[0];
                    perfShell[1] += performance[1];
                    j++;
                }

                perfSelection[0] = perfSelection[0]/j;
                perfSelection[1] = perfSelection[1]/j;
                perfInsertion[0] = perfInsertion[0]/j;
                perfInsertion[1] = perfInsertion[1]/j;
                perfBubble[0] = perfBubble[0]/j;
                perfBubble[1] = perfBubble[1]/j;
                perfShell[0] = perfShell[0]/j;
                perfShell[1] = perfShell[1]/j;

                writer.println( n +","+ j + "," + 
                                perfSelection[0] + "," + perfSelection[1] + "," +
                                perfInsertion[0] + "," + perfInsertion[1] + "," +
                                perfBubble[0] + "," + perfBubble[1] + "," +
                                perfShell[0] + "," + perfShell[1]);

                System.out.println("n = " + n + "/" + MAX_N);
            }
            
            writer.close();
        }
    }

    public static void TestBenchShell() {
        final int MIN_N = 1000;                    // Should not be lower than 1!
        final int MAX_N = 50000;
        final int N_STEP = 1000;
        final int MIN_ARRAY_VALUE = -100;
        final int MAX_ARRAY_VALUE = 100;
        final int ITERATIONS = 100;
        
        PrintWriter writer = null;
        
        try {
            writer = new PrintWriter("Shell_SortingAlgoritmsReport.csv", "UTF-8");           
        }
        catch (IOException  ex) {
            System.out.println("Exception: " + ex);
            if(writer != null){
                writer.close();
            }
        }
                
        if(writer != null){
            writer.println("N,Iterations,Avg Comp Selection,Avg Mov Selection," + 
                           "Avg Comp Shell,Avg Mov Shell");
            System.out.println("...%");
        
            for(int n = MIN_N; n <= MAX_N; n += N_STEP){
                long[] perfShell = {0,0};
                int[] performance;
                
                int j = 0;
                while(j<ITERATIONS) {
                    int[] arr = arrays.createIntArray(n, MIN_ARRAY_VALUE, MAX_ARRAY_VALUE);
                    
                    performance = sort.shell(arr, true);
                    perfShell[0] += performance[0];
                    perfShell[1] += performance[1];
                    j++;
                }

                perfShell[0] = perfShell[0]/j;
                perfShell[1] = perfShell[1]/j;

                writer.println( n +","+ j + "," + 
                                perfShell[0] + "," + perfShell[1]);

                System.out.println("n = " + n + "/" + MAX_N);
            }
            
            writer.close();
        }
    }    
    
    
    
    public static void TestBenchShell2(){
        String[][] students = new String[][]{
            {"729086", "Quiroga Alberto   ", "22", "Electronics"},
            {"721355", "Martinez Alejandro", "21", "Computer"},
            {"555846", "Guillermo Jennifer", "23", "Computer"},
            {"729087", "Espino Hector     ", "22", "Electronics"},
            {"729123", "Gutierrez Roberto ", "22", "Electronics"},
            {"729080", "Paez Jose Antonio ", "22", "Electronics"},
            {"728089", "Gonzalez Julian   ", "25", "Computer"}};
        
        System.out.println("Original Table:");
        for(String[] data : students)
            System.out.println(data[0] + " " + data[1] + " " + data[2] + " " + data[3]);
        
        System.out.println("Sorting... by Name");
        sort.shell2d(students, 1, true);
        
        for(String[] data : students)
            System.out.println(data[0] + " " + data[1] + " " + data[2] + " " + data[3]);
        
        System.out.println("Sorting... by Career");
        sort.shell2d(students, 3, true);
        
        for(String[] data : students)
            System.out.println(data[0] + " " + data[1] + " " + data[2] + " " + data[3]);
    }
    
    public static void tarea2_main(String[] args) {
        int N = 10;
        //int[] arr = createIntArray(N, -100, 100);
        int[] arr = {32,45,56,2,9,47,23,12,55,20};
        //int[] performance;
        
        arrays.printArray(arr);        
        //System.out.println(isSort(arr));
        
        //performance = selectionSort(arr, true);               
        //performance = insertionSort(arr, true);
        //performance = bubbleSort(arr, true);
        //performance = shellSort(arr, true);
        //System.out.println("N = " + N + " Comparisons = " +  performance[0] + " Swaps = " +  performance[1]);                
        //printArray(arr);
        
        //performance = selectionSort(arr, false);               
        //performance = insertionSort(arr, false);
        //performance = bubbleSort(arr, false);
        //performance = shellSort(arr, false);
        //System.out.println("N = " + N + " Comparisons = " +  performance[0] + " Swaps = " +  performance[1]);                
        //printArray(arr);
        
        //TestBench();
        
        //TestBenchShell();
        
        //TestBenchShell2();
    }
}
