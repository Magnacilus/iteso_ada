/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import iteso_ada.utils.AStar;

/**
 *
 * @author Administrador
 */
public class Tarea6 {
    public static void main(String[] args) { 
        boolean[][] maze = {
            {true, true, false, true, true, true},
            {false, true, false, true, false, false},
            {false, true, false, true, true, true},
            {true, true, true, false, true, false},
            {true, false, true, false, true, false},
            {true, false, true, false, true, false},
            {true, false, true, true, true, false},
            };
        
        int startR = 0;
        int startC = 0;
        int targetR = 0;
        int targetC = 5;
        
        System.out.println("Route from [" + startR + "," + startC + "] to [" + targetR + "," + targetC + "]:" );
        System.out.println(AStar.FindPath(maze, startR, startC, targetR, targetC));
    }    
}
