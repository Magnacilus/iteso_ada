/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import java.util.ArrayDeque;
import java.util.HashMap;

/**
 *
 * @author Administrador
 */
public class Tarea_5 {
    public static boolean DFS(boolean[][] graph, int start, int end){
        // Added fixed names for simplicity in order to avoid "map" dependency
        String names[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
        ArrayDeque<Integer> pendingNodes = new ArrayDeque<Integer>();
        ArrayDeque<Integer> route = new ArrayDeque<Integer>();
        boolean[] processed = new boolean[graph.length];
        
        pendingNodes.push(start); //DFS
        
        while(!pendingNodes.isEmpty()){
            
            int node = pendingNodes.pop(); //DFS
            route.push(node);
            
            if(node == end){
                String routeNodes = "";
                while(!route.isEmpty()){
                    routeNodes += names[route.pollLast()];
                }
                System.out.println(routeNodes);
                return true;
            }
            
            if(!processed[node]){                
                processed[node] = true;
                boolean onRoute = false;
                for(int i = 0; i<graph[node].length; i++){
                    if(graph[node][i]&&!processed[i]){
                        pendingNodes.push(i);  //DFS
                        onRoute = true;
                    }
                }
                
                if(!onRoute){
                    route.pop();
                }
            }
        }
        
        return false;
    }
    
    public static boolean BFS(boolean[][] graph, int start, int end){
        ArrayDeque<Integer> pendingNodes = new ArrayDeque<Integer>();
        boolean[] processed = new boolean[graph.length];
        
        /**
         * Inserts the specified element at the end of this deque.
         */
        pendingNodes.offer(start); //BFS
        
        while(!pendingNodes.isEmpty()){
            
            /**
             * Retrieves and removes the head of the queue represented by this 
             *  deque (in other words, the first element of this deque), 
             *  or returns null if this deque is empty.
             */
            int node = pendingNodes.poll(); //BFS 
            
            if(node == end){
                return true;
            }
            
            if(!processed[node]){                
                processed[node] = true;
                for(int i = 0; i<graph[node].length; i++){
                    if(graph[node][i]&&!processed[i]){
                        pendingNodes.offer(i);  //BFS
                    }
                }
            }
        }
        
        return false;
    }
    
    public static boolean testConnectivity(boolean[][] graph){
        boolean boEntireGraphConnected = true;        
        // Added fixed names for simplicity in order to avoid "map" dependency
        String names[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
        
        boolean[] connected = new boolean[graph[0].length];
        
        for(int i = 0; i < graph[0].length; i++){
            String group = names[i];
            for(int j = 0; j< graph[i].length; j++){
                if(j != i && !connected[j]){
                    if(DFS(graph, i, j)){
                        connected[j] = true;
                        group += names[j];   
                    }
                    else{
                        boEntireGraphConnected = false;
                    }
                }
            }
            
            if(!connected[i]){
                System.out.println(group);
            }
        }
        
        return boEntireGraphConnected;
    }
    
    public static void tarea_5_main_1(String[] args){
        String names[] = {"A", "B", "C", "D", "E", "F", "G"};
        boolean t = true, f = false;
                            //      A  B  C  D  E  F  G
        boolean[][] graph = {/*A*/{ f, t, t, f, f, f, f },
                             /*B*/{ t, f, t, f, f, f, f },
                             /*C*/{ t, t, f, f, f, f, f },
                             /*D*/{ f, f, f, f, t, t, f },
                             /*E*/{ f, f, f, t, f, f, t },
                             /*F*/{ f, f, f, t, f, f, t },
                             /*G*/{ f, f, f, f, t, t, f }
        };

//        String names[] = {"A", "B", "C", "D", "E"};
//        boolean t = true, f = false;
//                            //      A  B  C  D  E 
//        boolean[][] graph = {/*A*/{ f, t, f, f, f },
//                             /*B*/{ t, f, t, f, f },
//                             /*C*/{ f, t, f, f, f },
//                             /*D*/{ f, f, f, f, t },
//                             /*E*/{ f, f, f, t, f }
//        };

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for(int i = 0; i < names.length; i++)
            map.put(names[i], i);

//        System.out.println("Grafo conexo: " + testConnectivity(graph));
        System.out.println("Search: " + DFS(graph, map.getOrDefault("A", -1), map.getOrDefault("G", -1)));
    }    
}
