/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

/**
 *
 * @author Administrador
 */
public class HillClimbing {
    static double MIN_X1 = -5, MIN_X2 = -5;
    static double MAX_X1 = 5,  MAX_X2 = 5;
    //static double BEST_X1 = 1, BEST_X2 = 1;
    static double BEST_X1 = -2.903535, BEST_X2 = -2.903535;
    
   //Rosenbrock   
   static double R(double x1, double x2){
      return  100* Math.pow(x2-x1*x1, 2)+
                 Math.pow(1-x1, 2);
   }
   
   //Styblinskiand Tang
   static double ST(double x1, double x2){
      return   0.5*(Math.pow(x1,4)-16*x1*x1+5*x1+Math.pow(x2,4)-16*x2*x2+5*x2);
   }
   
   static double getFitness(double r){
      return 1.0 / (1+r);
   }
   
   static double rand(double min, double max){
      return min+ (max-min)* Math.random();
   }

   static double norm(double x, double y){
      return Math.sqrt(x*x + y*y);
   }
   
   /**
    * 
    * @param G  número de generaciones (iteraciones)
    * @param minDX  mínima diferencia esperada
    */
   static void hillClimbing(int G, double minDX){
      double x1 = rand(MIN_X1, MAX_X1);
      double x2 = rand(MIN_X2, MAX_X2);
      //valor de la función en puntos aleatorios
      //double r = R(x1,x2);
      double r = ST(x1,x2);
      //evaluar la solución
      double f = getFitness(r);
      int g  =0; //contador de generaciones
      
      double Dx1 = (MAX_X1-MIN_X1)*(1-f);
      double Dx2 = (MAX_X2-MIN_X2)*(1-f);
      
      while( g < G && norm(Dx1,Dx2) > minDX){
         double dx1 = rand(-Dx1/2, Dx1/2);
         double dx2 = rand(-Dx2/2, Dx2/2);
         //double r1 =  R(x1+dx1, x2+dx2);
         double r1 =  ST(x1+dx1, x2+dx2);
         if(r1 < r){
            r=r1;
            f = getFitness(r);
            x1+= dx1;
            x2+= dx2;
            Dx1 = (MAX_X1-MIN_X1)*(1-f);
            Dx2 = (MAX_X2-MIN_X2)*(1-f);
            //System.out.println(Dx1 + " "+ Dx2 + " f "+ f);
         }
         g++;
      }
      
      System.out.printf("Mejor solución: R(%.4f,%.4f) =%.7f\n",x1,x2,r);
      System.out.printf(" Generaciones = %d, fitness = %.4f\n",g+1,f);
      System.out.printf("Distancia con el optimo: %.4f\n", norm(x1-BEST_X1, x2-BEST_X2));
   }
   
   static void randomSolution(int N){
      double bestX1  =0, bestX2 = 0, bestR = Double.MAX_VALUE;
      
      for(int i =0; i<N; i++){
         double x1 = rand(MIN_X1, MAX_X1);
         double x2 = rand(MIN_X2, MAX_X2);
         //double r = R(x1, x2);
         double r = ST(x1, x2);
         if(r< bestR){
            bestR = r;
            bestX1 = x1;
            bestX2 = x2;
         }
         
      }
      
      System.out.printf("Mejor solución: R(%.4f,%.4f) =%.7f\n",bestX1,bestX2,bestR);
      System.out.printf("Distancia con el optimo: %.4f\n", norm(bestX1-BEST_X1, bestX2-BEST_X2));
      
   }
   
   public static void main(String[] args) {
      System.out.println("******HillClimbing***");
      hillClimbing(20_000, 0.000_001);
      System.out.println("******RANDOM***");
      randomSolution(20_000);
   }
}
