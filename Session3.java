/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import iteso_ada.utils.arrays;
import iteso_ada.utils.sort;

/**
 *
 * @author Administrador
 */
public class Session3 {

    public static void session3_main(String[] args) {
        // TODO code application logic here
        //int N = 1000000;
        int N = 10;
        int[] performance;
        int[] arr = arrays.createIntArray(N, -100, 100);
        
        arrays.printArray(arr);
        System.out.println("Sorted: " + arrays.isSort(arr, true));

        performance = sort.heap(arr);
        
        arrays.printArray(arr);
        System.out.println("Sorted: " + arrays.isSort(arr,true));
        
        System.out.println("N = " + N + " Comparisons = " +  performance[0] + " Swaps = " +  performance[1]); 
    }
}
