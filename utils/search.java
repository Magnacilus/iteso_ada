/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import iteso_ada.utils.binary_tree;

/**
 *
 * @author Administrador
 */
public class search {
    
/*-----------------------------------------------------------------------------

    Session4:   Binary Search, Media finding algorithms.

-----------------------------------------------------------------------------*/
    
    /*
    * Binary search implementation
    * PRE-REQUISIT: Array should be already oredered ***
    * arr: array containing the data.
    * inferior: Lower array index for the search window
    * superior: Higher array index for the search window
    * value: Value to be sarched.
    */  
    private static int binarySearch(int[] array, int min, int max, int val){
            if((max-min)<2){
                    return (array[min]==val)? min: -1;
            }
            int temp = min+(max-min)/2;
            if(array[temp]==val)
                    return temp;

            if(val<array[temp])
                    return binarySearch(array,min,temp,val);
            else
                    return binarySearch(array,temp,max,val);

    }

    public static int binarySearch(int[]array, int val){
            return binarySearch(array,0,array.length, val);
    }
    
    /*
    * Binary search iterative implementation
    * PRE-REQUISIT: Array should be already oredered ***
    * arr: array containing the data.
    * value: Value to be sarched.
    */  
    public static int binarySearchIte(int[] arr, int value){
        int start = 0;
        int end = arr.length - 1;
        while(true){            
            int middleIndex = start + (end-start)/2;
            if(arr[middleIndex]==value){
                return middleIndex;
            }
            else if(start==end){
                return -1;
            }
            else if (value <arr[middleIndex]){
                end = middleIndex - 1;                
            }
            else {
                start = middleIndex + 1;
            }                
        }        
    }
    
    /*
    * Binary search implementation with interpolation
    * PRE-REQUISIT: Array should be already oredered ***
    * arr: array containing the data.
    * inferior: Lower array index for the search window
    * superior: Higher array index for the search window
    * value: Value to be sarched.
    */  
    private static int binarySearchInterpolation(int[] array, int min, int max, int val, int[] performance){
        final int COMPARISONS = 0;
        final int SWAPS = 1;
        
        if((max-min)<2){
            performance[COMPARISONS]++;
            return (array[min]==val)? min: -1;
        }
        int temp = min+(max-min)*(val-array[min])/(array[max]-array[min]);
        if(array[temp]==val){
            return temp;
        }

        if(val<array[temp]){
            return binarySearch(array,min,temp,val);
        }
        else {
            return binarySearch(array,temp,max,val);
        }
    }

    public static int binarySearchInterpolation(int[]array, int val){
        return binarySearch(array,0,array.length, val);
    }

    /**
    * Identifies the Median into an array of integers.
    * PRE-REQUISIT: Array should not contain repeated items. ***
    * arr: array containing the data.
    * k: Array item where to start looking for the median 
    *   (normally at the middle)
    * performance: Returns the number of Comparisons and Datat Transfers
    *              performed during the algorithm execution.
    
     Número Mediana(L: lista de N elementos, K: posición esperada) 
        P = el número de elementos más pequeños que LN/2
        Si P = K, devolver LK
        Si P > K, En la mitad hay un número grande
                Crear una lista L1 con los elementos más pequeños que LN/2
                Devolver Mediana(L1, K)
        Si P < K,  En la mitad hay un número pequeño
                Crear una lista L2 con los elementos más grandes que LN/2
                Devolver  Mediana(L2, K – P – 1)
    */
    private static int Median(int[] arr, int k, int[] performance){
        final int COMPARISONS = 0;
        final int SWAPS = 1;
        int p = 0;
        int n2 = arr.length/2;
        
        for(int i = 0; i<arr.length; i++){
            performance[COMPARISONS]++;
            if(arr[i]<arr[n2]){ 
                p++;
            }
        }
            
        if(p==k){
            performance[COMPARISONS]++;
            return arr[k];
        }
        
        if(p>k){
            int[] arr1 = new int[p];
            for(int i = 0, j=0; i<arr.length; i++){
                performance[COMPARISONS]++;
                if(arr[i]<arr[n2]){
                    performance[SWAPS]++;
                    arr1[j++]=arr[i];
                }
            }
            return Median(arr1,k, performance);
        }
        else {
            int[] arr1 = new int[arr.length - p - 1];
            for(int i = 0, j=0; i<arr.length; i++){
                performance[COMPARISONS]++;
                if(arr[i]>arr[n2])
                {
                    performance[SWAPS]++;
                    arr1[j++]=arr[i];                
                }
            }
            return Median(arr1, k-p-1, performance);                    
        }
    }

    /*
    * Identifies the Median into an array of integers. This function sets the 
    * starting point to search in the middle of the array, then call the actual
    * median identification implementation.
    * Requirement: Array should not contain repeated items.
    * arr: array containing the data.
    */      
    public static int median(int[] arr, int[] performance){
        if(arr.length > 0){
            return Median(arr, arr.length/2, performance);
        }
        else {
            return 0;
        }
    }
    
    /*
    * Performs a data search using a binary tree structure
    * arr: array containing the data.
    * value: value to search.
    */
    public static int binaryTreeSearch(int[] array, int value){
        return binary_tree.doBinaryTreeSearch(array, value);
    }    
}
