/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

/**
 * Binary Tree class, that holds binaray tree strucutre and 
 * operations with binary tree structures.
 */
public class binary_tree {

    /**
    * Binary Tree Structure
    */
    public static class Node {
        public int value;
        public int index;
        public Node left=null, right=null;
        
        public Node(int value, int index){
            this.value=value;
            this.index = index;
        }
    }
    
    /**
    * Creates a Binary Tree from an array of integer values.
    * @param array: Base arry to create the binary tree.
    */
    public static Node createBinaryTree(int[] array){
        Node root = new Node(array[0], 0);
        
        for(int i = 1; i < array.length; i++){
            Node current = root;
            Node newNode = new Node(array[i],i);
            while(true){
                if(newNode.value < current.value){
                    if(current.left == null){
                        current.left = newNode;
                        break;
                    }
                    else{
                        current = current.left;
                    }
                }
                else{
                    if(current.right == null){
                        current.right = newNode;
                        break;
                    }
                    else{
                        current = current.right;
                    }
                }
            }
        }
        
        return root;
    }
    
    /**
    * Performs a data search into a binary tree.
    * @param root: Root node fo the binary tree.
    * @param value: Value to search.
    */
    private static int binaryTreeSearch(Node root, int value){
        if(root == null){
            return -1;
        }
        if(root.value == value){
            return root.index;
        }
        else if (value < root.value){
            return binaryTreeSearch(root.left, value);            
        }
        else {
            return binaryTreeSearch(root.right, value);
        }
    }
    
    /**
    * Performs a data search operation by creating and using
    * a binary tree structure
    * @param array: Array of values where the search should be performed.
    * @param value: Data value to be serached.
    */
    public static int doBinaryTreeSearch(int[] array, int value){
        Node root = createBinaryTree(array);
        return binaryTreeSearch(root, value);
    }
    
    public static void print(Node n, int spaces){
        if(n==null) return;
        for(int i=0; i<spaces; i++){
            System.out.print("");            
        }
        
        System.out.println("["+n.index+"], " + n.value);
        
        print(n.left, spaces + 1);
        print(n.right, spaces + 1);
    }
}