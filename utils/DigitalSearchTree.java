/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

/**
 *
 * @author Administrador
 */
public class DigitalSearchTree {
    static int getBit(int value, int bitIndex){
        return (value>>bitIndex) & 0x01;
    }
    
    public static class NodeDT{
        int value;
        int index;
        NodeDT left = null;
        NodeDT right = null;
        
        public NodeDT(int value, int index){
            this.value = value;
            this.index = index;
        }
    }
        
    public static NodeDT createDT(int[] array){
        NodeDT root = new NodeDT(array[0], 0);
        for(int i = 1; i<array.length; i++){
            NodeDT current = root;
            NodeDT newNode = new NodeDT(array[i], i);
            boolean nodeAdded = false;
            int bitIndex = 0;
            while(!nodeAdded){
                int newNodeBit = getBit(array[i], bitIndex++);
                if(newNodeBit == 0){
                    if(current.left == null){
                        current.left = newNode;
                        nodeAdded = true;
                    }
                    else{
                        current = current.left;
                    }                    
                }
                else{
                    if(current.right == null){
                        current.right = newNode;
                        nodeAdded = true;
                    }
                    else{
                        current = current.right;
                    }
                }
            }
        }

        return root;
    }
    
    public static int searchDT(NodeDT root, int value, int bitIndex){
        if(root == null) return -1;
        if(root.value==value) return root.index;
        if(getBit(value,bitIndex)==0)
            return searchDT(root.left, value, bitIndex + 1);
        else
            return searchDT(root.right, value, bitIndex + 1);
        
    }

    public static void printDT(NodeDT current, String spaces){            
        if(current == null) return;

        System.out.println(spaces + current.value);
        printDT(current.left, spaces+" ");
        printDT(current.right, spaces+" ");
    }
}
