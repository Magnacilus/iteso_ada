/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada.utils;

import java.util.Arrays;

/**
 *
 * @author Administrador
 */
public class NandPerceptron {
    
    //feed forward
    static double getOutput(byte[] input, double[] weights){
        double output = 0;
        for(int i = 0; i < input.length; i++){
            output += input[i]*weights[i];
        }
        return output;
    }
    
    static byte activationFunction(double output){
        return output > 0.5 ? (byte)1 : (byte)0;
    }
    
    static void backPropagation(double error, byte[] input, double[] weights){
        final double LEARNING_RATE = 0.1;
        double delta = error * LEARNING_RATE;
        for(int i = 0; i < input.length; i++){
            weights[i] += input[i]*delta;
        }
    }
    
    static double[] trainPerceptron(byte[][] trainingMatrix, byte[] classes){
        double[] weights = new double[trainingMatrix[0].length];
        int currentRow = 0;
        int iteration = 0;
        int hits = 0; //Cuantos han pasado la prueba
        int count = 0;
        
        while(trainingMatrix.length > hits){            
            byte[] currentInput = trainingMatrix[currentRow];
            double output = getOutput(currentInput, weights);            
            byte af = activationFunction(output);
            double error = classes[currentRow]-af;
            System.out.println(count++ + ": " + Arrays.toString(currentInput) + 
                    " - " + Arrays.toString(weights) + " - Err:" + error);
            if(error == 0){
                hits++;
            }
            else{
                hits = 0;
                backPropagation(error, currentInput, weights);
            }
            
            currentRow++;
            iteration++;
            currentRow %= trainingMatrix.length;
        }
        
        return weights;
    }
    
    public static void main(String[] args) {
        byte [][] trainingMatrix = {{1, 0, 0, 0},
                                    {1, 0, 0, 1},
                                    {1, 0, 1, 0},
                                    {1, 0, 1, 1},
                                    {1, 1, 1, 1}};

        byte[] classes = {1, 1, 1, 1, 0};
        double[] perceptron = trainPerceptron(trainingMatrix, classes);
        
        System.out.println("Weights: " + Arrays.toString(perceptron));
        
        int inputs = 3;
        byte output = activationFunction(getOutput(new byte[] {1, 0, 1, 1}, perceptron));
        System.out.println("Solution: " + output);
    }
}
