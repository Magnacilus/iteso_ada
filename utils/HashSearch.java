
package iteso_ada.utils;

import java.io.BufferedReader;
import java.io.FileReader;

public class HashSearch {

	static int HASH_LIST_SIZE = 0;
	
	
	static class Customer {
		String rfc;
		String name, address;
		int index;
		
		public Customer(String rfc, String name, String address, int index) {
			this.rfc     = rfc;
			this.name    = name;
			this.address = address;
			this.index   = index;
		}
	}
	
	
	static Customer[] readCustomers(String filename) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line = br.readLine().trim();
		final int COUNT = Integer.parseInt(line);
		Customer[] customers = new Customer[COUNT];
		for(int i = 0; i < COUNT; i ++) {
			line = br.readLine();
			String[] rowData = line.split("\t");
			customers[i] = new Customer(rowData[1].trim(), rowData[0].trim(), rowData[2].trim(), i);
		}
		br.close();
		return customers;
	}
        
        static int getValue(char c){
            
            if(Character.isDigit(c))
                return c - '0';
            
            return c-'A' + 10;
        }
        
        static int hashCode(String rfc){
            int h = getValue(rfc.charAt(0));
            for(int i=1; i<rfc.length(); i++){
                h=(h*36 + getValue(rfc.charAt(i))) % HASH_LIST_SIZE;
            }
            return h;
        }
        
        static class CustomerNode {
            Customer customer;
            CustomerNode next;
            
            CustomerNode(Customer customer){
                this.customer = customer;
                this.next = null;
            }
        }
        
        static CustomerNode[] createCustomerHashList(Customer[] customers){
            CustomerNode[] hashList = new CustomerNode[HASH_LIST_SIZE];
            for(Customer c: customers){
                int h = hashCode(c.rfc);
                CustomerNode newNode = new CustomerNode(c);
                CustomerNode current = hashList[h];
                if(current == null){
                    hashList[h] = newNode;
                }
                else{
                    newNode.next = hashList[h];
                    hashList[h] = newNode;
                }
            }
            
            return hashList;
        }
	
	static boolean searchCustomer(CustomerNode[] hashList, String rfc){
            int h = hashCode(rfc);
            CustomerNode cn = hashList[h];
            
            while (cn != null){
                if(cn.customer.rfc.equals(rfc)){
                    return true;
                }
                else{
                    System.out.println("Jumped...");
                    cn = cn.next;
                }
            }
            
            return false;
        }
        
	public static void main(String[] args) throws Exception {
		Customer[] customers = readCustomers("C:\\Data\\ITESO\\2016_Herbst\\ADA\\projects\\ITESO_ADA\\src\\iteso_ada\\utils\\Clientes.txt");
		HASH_LIST_SIZE = customers.length;
		System.out.println(customers.length);
                
		
                CustomerNode[] hashList = createCustomerHashList(customers);
                int customerIndex = 13;
                System.out.println(hashCode(customers[customerIndex].rfc));
                System.out.println("RFC: " + customers[customerIndex].rfc + " Preset? " + searchCustomer(hashList, customers[customerIndex].rfc));
                
	}		
}
