/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import iteso_ada.utils.arrays;
import iteso_ada.utils.search;
import iteso_ada.utils.sort;

/**
 *
 * @author Administrador
 */
public class Session5 {
    
    public static void session5_main(String[] args) {
        int N = 10;
        //int[] arr = arrays.createSortedIntArray(N, 5);
        int[] arr = arrays.createIntArray(N, -100, 100);
        //int[] arr = {2, 5, 6, 7, 11, 12, 14, 14, 15, 16};
        //int[] arr = {2, 16, 16, 16, 11, 12, 14, 14, 15, 17};
        //int[] performance;
        
        arrays.printArray(arr);
        //int value = 11;
        //int index = search.binarySearchIte(arr, value);
        //int index = search.binarySearchInterpolation(arr, value);
        //int index = search.binaryTreeSearch(arr, value);
        //System.out.println("Value " + value + " Index = " + index);                
        
        sort.mergeSortIte(arr);               
        //System.out.println("N = " + N + " Comparisons = " +  performance[0] + " Swaps = " +  performance[1]);                
        arrays.printArray(arr);
    }
    
}
