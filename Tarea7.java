/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import java.util.Scanner;

/**
 *
 * @author Administrador
 */
public class Tarea7 {
    static final int COIN_VALUE = 0, COIN_AMOUNT = 1;
    
    static void coinChange(int change, int[][] coins){       
        /* Populate Coins Table */
        int[][] coinsTable = new int[coins.length][change+1];
        for(int coinIndex = 0; coinIndex < coins.length; coinIndex++){
            for(int currentChange = 0; currentChange < change + 1; currentChange++){
                int changeCoins = 0;
                int coins_amount = currentChange / coins[coinIndex][COIN_VALUE];
                                
                if(coinIndex==0){/* Assuming sorted coins and the first coin has a unit value */
                    changeCoins = coins_amount <= coins[coinIndex][COIN_AMOUNT] ? coins_amount : coins[coinIndex][COIN_AMOUNT] ;
                }                    
                else if(coins_amount == 0){ /* Current change is lower than current coin value, then set previous row value */
                    changeCoins = coinsTable[coinIndex-1][currentChange];
                }
                else if(coins_amount > coins[coinIndex][COIN_AMOUNT]){ /* Current change is grater than available coins */
                    /* Take available coins */
                    changeCoins = coins[coinIndex][COIN_AMOUNT];
                    /* Add coins from previous row in order to complete the remaining change*/
                    changeCoins += coinsTable[coinIndex-1][currentChange - changeCoins * coins[coinIndex][COIN_VALUE]];
                }
                else {  /* Current change is not grather than available coins */
                    /* Take needed coins */
                    changeCoins = coins_amount;
                    /* Check for possible remaining change fraction */
                    int remainingChange = currentChange % coins[coinIndex][COIN_VALUE];
                    if(remainingChange > 0){
                        /* Add coins from previous row in order to complete the remaining change*/
                        changeCoins += coinsTable[coinIndex-1][remainingChange];
                    }
                }
                
                coinsTable[coinIndex][currentChange] = changeCoins;
            }
        }
        
        /* Calculate change based on Coins Table */
        int remainingChange = change;
        int coinIndex = coins.length - 1;
        int[] usedCoins = new int[coins.length];
        
        while(coinIndex >= 0){
            if(remainingChange > 0){
                if(coinIndex == 0){
                    usedCoins[coinIndex] = coinsTable[coinIndex][remainingChange];
                    remainingChange = 0;
                }
                else if(coinsTable[coinIndex][remainingChange] < coinsTable[coinIndex - 1][remainingChange]){
                    usedCoins[coinIndex]++;
                    remainingChange -= coins[coinIndex][COIN_VALUE];
                    if(usedCoins[coinIndex] >= coins[coinIndex][COIN_AMOUNT]){
                        System.out.println(Integer.toString(coins[coinIndex][COIN_VALUE]) + " " + Integer.toString(usedCoins[coinIndex]));
                        coinIndex--;
                    }
                }
                else{
                    System.out.println(Integer.toString(coins[coinIndex][COIN_VALUE]) + " " + Integer.toString(usedCoins[coinIndex]));
                    coinIndex--;
                }
            }
            else{ /* Just for printing purposes */
                System.out.println(Integer.toString(coins[coinIndex][COIN_VALUE]) + " " + Integer.toString(usedCoins[coinIndex]));
                coinIndex--;
            }
        }
    }
    
    public static void main(String[] args) {
        /* Dynamic Input */
        Scanner sc = new Scanner(System.in);
        int coinsLength = sc.nextInt();
        int change = sc.nextInt();
        
        int[][] coins = new int[coinsLength][2];
        
        for(int i = 0; i < coinsLength; i++)
            coins[i][COIN_VALUE] = sc.nextInt();
        for(int i = 0; i < coinsLength; i++)
            coins[i][COIN_AMOUNT] = sc.nextInt();

        /* Static Input */
//        int coinsLength = 4;
//        int change = 10;
//        int[][] coins = {{1, 100},
//                         {2, 2},
//                         {3, 2},
//                         {4, 1}
//                        };
        
        coinChange(change, coins);
   }
}
