/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteso_ada;

import iteso_ada.utils.sort;

import java.util.Scanner;


/**
 *
 * @author Administrador
 */
public class Tarea_4_1 {
    
    public static void SwapTrain(){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        
        for(int n = 0; n < N; n ++) {
            int length = sc.nextInt();
            int[] train = new int[length];
            int[] performance;
            for(int j=0; j<length; j++){
                train[j] = sc.nextInt();
            }
            performance = sort.bubble(train, true);
            System.out.println("Optimal train swapping takes " +  performance[1] + " swaps");
        }
    }   
   
}
